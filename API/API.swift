//  This file was automatically generated and should not be edited.

import Apollo
import Foundation

public final class CreateStreamingTicketMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition =
    """
    mutation createStreamingTicket($uuid: String!) {
      createStreamingTicket(uuid: $uuid) {
        __typename
        error {
          __typename
          hasError
          message
        }
        streams {
          __typename
          language
          title
          streamType
          streamID
          streamURL
        }
        metadataPath
        dashStreamingPath
        hlsStreamingPath
        jwt
      }
    }
    """

  public let operationName = "createStreamingTicket"

  public var uuid: String

  public init(uuid: String) {
    self.uuid = uuid
  }

  public var variables: GraphQLMap? {
    return ["uuid": uuid]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("createStreamingTicket", arguments: ["uuid": GraphQLVariable("uuid")], type: .nonNull(.object(CreateStreamingTicket.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(createStreamingTicket: CreateStreamingTicket) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "createStreamingTicket": createStreamingTicket.resultMap])
    }

    /// Request permission to play a certain file
    public var createStreamingTicket: CreateStreamingTicket {
      get {
        return CreateStreamingTicket(unsafeResultMap: resultMap["createStreamingTicket"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "createStreamingTicket")
      }
    }

    public struct CreateStreamingTicket: GraphQLSelectionSet {
      public static let possibleTypes = ["CreateSTResponse"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("error", type: .object(Error.selections)),
        GraphQLField("streams", type: .nonNull(.list(.object(Stream.selections)))),
        GraphQLField("metadataPath", type: .nonNull(.scalar(String.self))),
        GraphQLField("dashStreamingPath", type: .nonNull(.scalar(String.self))),
        GraphQLField("hlsStreamingPath", type: .nonNull(.scalar(String.self))),
        GraphQLField("jwt", type: .nonNull(.scalar(String.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(error: Error? = nil, streams: [Stream?], metadataPath: String, dashStreamingPath: String, hlsStreamingPath: String, jwt: String) {
        self.init(unsafeResultMap: ["__typename": "CreateSTResponse", "error": error.flatMap { (value: Error) -> ResultMap in value.resultMap }, "streams": streams.map { (value: Stream?) -> ResultMap? in value.flatMap { (value: Stream) -> ResultMap in value.resultMap } }, "metadataPath": metadataPath, "dashStreamingPath": dashStreamingPath, "hlsStreamingPath": hlsStreamingPath, "jwt": jwt])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var error: Error? {
        get {
          return (resultMap["error"] as? ResultMap).flatMap { Error(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "error")
        }
      }

      public var streams: [Stream?] {
        get {
          return (resultMap["streams"] as! [ResultMap?]).map { (value: ResultMap?) -> Stream? in value.flatMap { (value: ResultMap) -> Stream in Stream(unsafeResultMap: value) } }
        }
        set {
          resultMap.updateValue(newValue.map { (value: Stream?) -> ResultMap? in value.flatMap { (value: Stream) -> ResultMap in value.resultMap } }, forKey: "streams")
        }
      }

      public var metadataPath: String {
        get {
          return resultMap["metadataPath"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "metadataPath")
        }
      }

      public var dashStreamingPath: String {
        get {
          return resultMap["dashStreamingPath"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "dashStreamingPath")
        }
      }

      /// Path with a JWT that will stream your file.
      public var hlsStreamingPath: String {
        get {
          return resultMap["hlsStreamingPath"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "hlsStreamingPath")
        }
      }

      public var jwt: String {
        get {
          return resultMap["jwt"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "jwt")
        }
      }

      public struct Error: GraphQLSelectionSet {
        public static let possibleTypes = ["Error"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("hasError", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("message", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(hasError: Bool, message: String) {
          self.init(unsafeResultMap: ["__typename": "Error", "hasError": hasError, "message": message])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var hasError: Bool {
          get {
            return resultMap["hasError"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "hasError")
          }
        }

        public var message: String {
          get {
            return resultMap["message"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "message")
          }
        }
      }

      public struct Stream: GraphQLSelectionSet {
        public static let possibleTypes = ["Stream"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("language", type: .scalar(String.self)),
          GraphQLField("title", type: .scalar(String.self)),
          GraphQLField("streamType", type: .scalar(String.self)),
          GraphQLField("streamID", type: .scalar(Int.self)),
          GraphQLField("streamURL", type: .scalar(String.self)),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(language: String? = nil, title: String? = nil, streamType: String? = nil, streamId: Int? = nil, streamUrl: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Stream", "language": language, "title": title, "streamType": streamType, "streamID": streamId, "streamURL": streamUrl])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        /// Language used for audio or subtitle types
        public var language: String? {
          get {
            return resultMap["language"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "language")
          }
        }

        /// Title for audio and subtitle streams
        public var title: String? {
          get {
            return resultMap["title"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "title")
          }
        }

        /// Type of stream can be either 'video', 'audio' or 'subtitle'
        public var streamType: String? {
          get {
            return resultMap["streamType"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "streamType")
          }
        }

        /// Stream/Track ID as found in the original file
        public var streamId: Int? {
          get {
            return resultMap["streamID"] as? Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "streamID")
          }
        }

        /// StreamURL
        public var streamUrl: String? {
          get {
            return resultMap["streamURL"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "streamURL")
          }
        }
      }
    }
  }
}

public final class MovieListQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition =
    """
    query MovieList($offset: Int, $limit: Int) {
      movies(offset: $offset, limit: $limit) {
        __typename
        name
        posterPath
        uuid
        overview
        playState {
          __typename
          finished
          playtime
        }
        files {
          __typename
          totalDuration
          fileName
          uuid
        }
      }
    }
    """

  public let operationName = "MovieList"

  public var offset: Int?
  public var limit: Int?

  public init(offset: Int? = nil, limit: Int? = nil) {
    self.offset = offset
    self.limit = limit
  }

  public var variables: GraphQLMap? {
    return ["offset": offset, "limit": limit]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("movies", arguments: ["offset": GraphQLVariable("offset"), "limit": GraphQLVariable("limit")], type: .nonNull(.list(.object(Movie.selections)))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(movies: [Movie?]) {
      self.init(unsafeResultMap: ["__typename": "Query", "movies": movies.map { (value: Movie?) -> ResultMap? in value.flatMap { (value: Movie) -> ResultMap in value.resultMap } }])
    }

    public var movies: [Movie?] {
      get {
        return (resultMap["movies"] as! [ResultMap?]).map { (value: ResultMap?) -> Movie? in value.flatMap { (value: ResultMap) -> Movie in Movie(unsafeResultMap: value) } }
      }
      set {
        resultMap.updateValue(newValue.map { (value: Movie?) -> ResultMap? in value.flatMap { (value: Movie) -> ResultMap in value.resultMap } }, forKey: "movies")
      }
    }

    public struct Movie: GraphQLSelectionSet {
      public static let possibleTypes = ["Movie"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .nonNull(.scalar(String.self))),
        GraphQLField("posterPath", type: .nonNull(.scalar(String.self))),
        GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
        GraphQLField("overview", type: .nonNull(.scalar(String.self))),
        GraphQLField("playState", type: .object(PlayState.selections)),
        GraphQLField("files", type: .nonNull(.list(.object(File.selections)))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(name: String, posterPath: String, uuid: String, overview: String, playState: PlayState? = nil, files: [File?]) {
        self.init(unsafeResultMap: ["__typename": "Movie", "name": name, "posterPath": posterPath, "uuid": uuid, "overview": overview, "playState": playState.flatMap { (value: PlayState) -> ResultMap in value.resultMap }, "files": files.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      /// Official title according to the MovieDB
      public var name: String {
        get {
          return resultMap["name"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }

      /// ID to retrieve poster
      public var posterPath: String {
        get {
          return resultMap["posterPath"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "posterPath")
        }
      }

      public var uuid: String {
        get {
          return resultMap["uuid"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "uuid")
        }
      }

      /// Short description of the movie
      public var overview: String {
        get {
          return resultMap["overview"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "overview")
        }
      }

      public var playState: PlayState? {
        get {
          return (resultMap["playState"] as? ResultMap).flatMap { PlayState(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "playState")
        }
      }

      public var files: [File?] {
        get {
          return (resultMap["files"] as! [ResultMap?]).map { (value: ResultMap?) -> File? in value.flatMap { (value: ResultMap) -> File in File(unsafeResultMap: value) } }
        }
        set {
          resultMap.updateValue(newValue.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } }, forKey: "files")
        }
      }

      public struct PlayState: GraphQLSelectionSet {
        public static let possibleTypes = ["PlayState"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("finished", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("playtime", type: .nonNull(.scalar(Double.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(finished: Bool, playtime: Double) {
          self.init(unsafeResultMap: ["__typename": "PlayState", "finished": finished, "playtime": playtime])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var finished: Bool {
          get {
            return resultMap["finished"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "finished")
          }
        }

        public var playtime: Double {
          get {
            return resultMap["playtime"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "playtime")
          }
        }
      }

      public struct File: GraphQLSelectionSet {
        public static let possibleTypes = ["MovieFile"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("totalDuration", type: .scalar(Double.self)),
          GraphQLField("fileName", type: .nonNull(.scalar(String.self))),
          GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(totalDuration: Double? = nil, fileName: String, uuid: String) {
          self.init(unsafeResultMap: ["__typename": "MovieFile", "totalDuration": totalDuration, "fileName": fileName, "uuid": uuid])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        /// Total duration of the first video stream in seconds
        public var totalDuration: Double? {
          get {
            return resultMap["totalDuration"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "totalDuration")
          }
        }

        /// Filename
        public var fileName: String {
          get {
            return resultMap["fileName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "fileName")
          }
        }

        public var uuid: String {
          get {
            return resultMap["uuid"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "uuid")
          }
        }
      }
    }
  }
}

public final class EpisodesForSeasonQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition =
    """
    query EpisodesForSeason($seasonUUID: String) {
      season(uuid: $seasonUUID) {
        __typename
        episodes {
          __typename
          uuid
          episodeNumber
          name
          overview
          tmdbID
          stillPath
          airDate
          playState {
            __typename
            uuid
            playtime
            finished
          }
          files {
            __typename
            uuid
            totalDuration
          }
        }
      }
    }
    """

  public let operationName = "EpisodesForSeason"

  public var seasonUUID: String?

  public init(seasonUUID: String? = nil) {
    self.seasonUUID = seasonUUID
  }

  public var variables: GraphQLMap? {
    return ["seasonUUID": seasonUUID]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("season", arguments: ["uuid": GraphQLVariable("seasonUUID")], type: .nonNull(.object(Season.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(season: Season) {
      self.init(unsafeResultMap: ["__typename": "Query", "season": season.resultMap])
    }

    public var season: Season {
      get {
        return Season(unsafeResultMap: resultMap["season"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "season")
      }
    }

    public struct Season: GraphQLSelectionSet {
      public static let possibleTypes = ["Season"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("episodes", type: .nonNull(.list(.object(Episode.selections)))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(episodes: [Episode?]) {
        self.init(unsafeResultMap: ["__typename": "Season", "episodes": episodes.map { (value: Episode?) -> ResultMap? in value.flatMap { (value: Episode) -> ResultMap in value.resultMap } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var episodes: [Episode?] {
        get {
          return (resultMap["episodes"] as! [ResultMap?]).map { (value: ResultMap?) -> Episode? in value.flatMap { (value: ResultMap) -> Episode in Episode(unsafeResultMap: value) } }
        }
        set {
          resultMap.updateValue(newValue.map { (value: Episode?) -> ResultMap? in value.flatMap { (value: Episode) -> ResultMap in value.resultMap } }, forKey: "episodes")
        }
      }

      public struct Episode: GraphQLSelectionSet {
        public static let possibleTypes = ["Episode"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
          GraphQLField("episodeNumber", type: .nonNull(.scalar(Int.self))),
          GraphQLField("name", type: .nonNull(.scalar(String.self))),
          GraphQLField("overview", type: .nonNull(.scalar(String.self))),
          GraphQLField("tmdbID", type: .nonNull(.scalar(Int.self))),
          GraphQLField("stillPath", type: .nonNull(.scalar(String.self))),
          GraphQLField("airDate", type: .nonNull(.scalar(String.self))),
          GraphQLField("playState", type: .object(PlayState.selections)),
          GraphQLField("files", type: .nonNull(.list(.object(File.selections)))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(uuid: String, episodeNumber: Int, name: String, overview: String, tmdbId: Int, stillPath: String, airDate: String, playState: PlayState? = nil, files: [File?]) {
          self.init(unsafeResultMap: ["__typename": "Episode", "uuid": uuid, "episodeNumber": episodeNumber, "name": name, "overview": overview, "tmdbID": tmdbId, "stillPath": stillPath, "airDate": airDate, "playState": playState.flatMap { (value: PlayState) -> ResultMap in value.resultMap }, "files": files.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var uuid: String {
          get {
            return resultMap["uuid"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "uuid")
          }
        }

        public var episodeNumber: Int {
          get {
            return resultMap["episodeNumber"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "episodeNumber")
          }
        }

        public var name: String {
          get {
            return resultMap["name"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var overview: String {
          get {
            return resultMap["overview"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "overview")
          }
        }

        public var tmdbId: Int {
          get {
            return resultMap["tmdbID"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "tmdbID")
          }
        }

        public var stillPath: String {
          get {
            return resultMap["stillPath"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "stillPath")
          }
        }

        public var airDate: String {
          get {
            return resultMap["airDate"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "airDate")
          }
        }

        public var playState: PlayState? {
          get {
            return (resultMap["playState"] as? ResultMap).flatMap { PlayState(unsafeResultMap: $0) }
          }
          set {
            resultMap.updateValue(newValue?.resultMap, forKey: "playState")
          }
        }

        public var files: [File?] {
          get {
            return (resultMap["files"] as! [ResultMap?]).map { (value: ResultMap?) -> File? in value.flatMap { (value: ResultMap) -> File in File(unsafeResultMap: value) } }
          }
          set {
            resultMap.updateValue(newValue.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } }, forKey: "files")
          }
        }

        public struct PlayState: GraphQLSelectionSet {
          public static let possibleTypes = ["PlayState"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
            GraphQLField("playtime", type: .nonNull(.scalar(Double.self))),
            GraphQLField("finished", type: .nonNull(.scalar(Bool.self))),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(uuid: String, playtime: Double, finished: Bool) {
            self.init(unsafeResultMap: ["__typename": "PlayState", "uuid": uuid, "playtime": playtime, "finished": finished])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var uuid: String {
            get {
              return resultMap["uuid"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "uuid")
            }
          }

          public var playtime: Double {
            get {
              return resultMap["playtime"]! as! Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "playtime")
            }
          }

          public var finished: Bool {
            get {
              return resultMap["finished"]! as! Bool
            }
            set {
              resultMap.updateValue(newValue, forKey: "finished")
            }
          }
        }

        public struct File: GraphQLSelectionSet {
          public static let possibleTypes = ["EpisodeFile"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
            GraphQLField("totalDuration", type: .scalar(Double.self)),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(uuid: String, totalDuration: Double? = nil) {
            self.init(unsafeResultMap: ["__typename": "EpisodeFile", "uuid": uuid, "totalDuration": totalDuration])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          public var uuid: String {
            get {
              return resultMap["uuid"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "uuid")
            }
          }

          /// Total duration of the first video stream in seconds
          public var totalDuration: Double? {
            get {
              return resultMap["totalDuration"] as? Double
            }
            set {
              resultMap.updateValue(newValue, forKey: "totalDuration")
            }
          }
        }
      }
    }
  }
}

public final class MovieDetailQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition =
    """
    query MovieDetail($uuid: String) {
      movie: movies(uuid: $uuid) {
        __typename
        name
        backdropPath
        posterPath
        overview
        uuid
        year
        playState {
          __typename
          finished
          playtime
        }
        files {
          __typename
          totalDuration
          fileName
          uuid
        }
      }
    }
    """

  public let operationName = "MovieDetail"

  public var uuid: String?

  public init(uuid: String? = nil) {
    self.uuid = uuid
  }

  public var variables: GraphQLMap? {
    return ["uuid": uuid]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("movies", alias: "movie", arguments: ["uuid": GraphQLVariable("uuid")], type: .nonNull(.list(.object(Movie.selections)))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(movie: [Movie?]) {
      self.init(unsafeResultMap: ["__typename": "Query", "movie": movie.map { (value: Movie?) -> ResultMap? in value.flatMap { (value: Movie) -> ResultMap in value.resultMap } }])
    }

    public var movie: [Movie?] {
      get {
        return (resultMap["movie"] as! [ResultMap?]).map { (value: ResultMap?) -> Movie? in value.flatMap { (value: ResultMap) -> Movie in Movie(unsafeResultMap: value) } }
      }
      set {
        resultMap.updateValue(newValue.map { (value: Movie?) -> ResultMap? in value.flatMap { (value: Movie) -> ResultMap in value.resultMap } }, forKey: "movie")
      }
    }

    public struct Movie: GraphQLSelectionSet {
      public static let possibleTypes = ["Movie"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .nonNull(.scalar(String.self))),
        GraphQLField("backdropPath", type: .nonNull(.scalar(String.self))),
        GraphQLField("posterPath", type: .nonNull(.scalar(String.self))),
        GraphQLField("overview", type: .nonNull(.scalar(String.self))),
        GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
        GraphQLField("year", type: .nonNull(.scalar(String.self))),
        GraphQLField("playState", type: .object(PlayState.selections)),
        GraphQLField("files", type: .nonNull(.list(.object(File.selections)))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(name: String, backdropPath: String, posterPath: String, overview: String, uuid: String, year: String, playState: PlayState? = nil, files: [File?]) {
        self.init(unsafeResultMap: ["__typename": "Movie", "name": name, "backdropPath": backdropPath, "posterPath": posterPath, "overview": overview, "uuid": uuid, "year": year, "playState": playState.flatMap { (value: PlayState) -> ResultMap in value.resultMap }, "files": files.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      /// Official title according to the MovieDB
      public var name: String {
        get {
          return resultMap["name"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }

      /// ID to retrieve backdrop
      public var backdropPath: String {
        get {
          return resultMap["backdropPath"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "backdropPath")
        }
      }

      /// ID to retrieve poster
      public var posterPath: String {
        get {
          return resultMap["posterPath"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "posterPath")
        }
      }

      /// Short description of the movie
      public var overview: String {
        get {
          return resultMap["overview"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "overview")
        }
      }

      public var uuid: String {
        get {
          return resultMap["uuid"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "uuid")
        }
      }

      /// Release year
      public var year: String {
        get {
          return resultMap["year"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "year")
        }
      }

      public var playState: PlayState? {
        get {
          return (resultMap["playState"] as? ResultMap).flatMap { PlayState(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "playState")
        }
      }

      public var files: [File?] {
        get {
          return (resultMap["files"] as! [ResultMap?]).map { (value: ResultMap?) -> File? in value.flatMap { (value: ResultMap) -> File in File(unsafeResultMap: value) } }
        }
        set {
          resultMap.updateValue(newValue.map { (value: File?) -> ResultMap? in value.flatMap { (value: File) -> ResultMap in value.resultMap } }, forKey: "files")
        }
      }

      public struct PlayState: GraphQLSelectionSet {
        public static let possibleTypes = ["PlayState"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("finished", type: .nonNull(.scalar(Bool.self))),
          GraphQLField("playtime", type: .nonNull(.scalar(Double.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(finished: Bool, playtime: Double) {
          self.init(unsafeResultMap: ["__typename": "PlayState", "finished": finished, "playtime": playtime])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var finished: Bool {
          get {
            return resultMap["finished"]! as! Bool
          }
          set {
            resultMap.updateValue(newValue, forKey: "finished")
          }
        }

        public var playtime: Double {
          get {
            return resultMap["playtime"]! as! Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "playtime")
          }
        }
      }

      public struct File: GraphQLSelectionSet {
        public static let possibleTypes = ["MovieFile"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("totalDuration", type: .scalar(Double.self)),
          GraphQLField("fileName", type: .nonNull(.scalar(String.self))),
          GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(totalDuration: Double? = nil, fileName: String, uuid: String) {
          self.init(unsafeResultMap: ["__typename": "MovieFile", "totalDuration": totalDuration, "fileName": fileName, "uuid": uuid])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        /// Total duration of the first video stream in seconds
        public var totalDuration: Double? {
          get {
            return resultMap["totalDuration"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "totalDuration")
          }
        }

        /// Filename
        public var fileName: String {
          get {
            return resultMap["fileName"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "fileName")
          }
        }

        public var uuid: String {
          get {
            return resultMap["uuid"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "uuid")
          }
        }
      }
    }
  }
}

public final class SeriesDetailQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition =
    """
    query SeriesDetail($uuid: String) {
      series(uuid: $uuid) {
        __typename
        name
        overview
        posterPath
        backdropPath
        status
        firstAirDate
        type
        uuid
        seasons {
          __typename
          uuid
          name
          overview
          seasonNumber
          airDate
          posterPath
        }
      }
    }
    """

  public let operationName = "SeriesDetail"

  public var uuid: String?

  public init(uuid: String? = nil) {
    self.uuid = uuid
  }

  public var variables: GraphQLMap? {
    return ["uuid": uuid]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("series", arguments: ["uuid": GraphQLVariable("uuid")], type: .nonNull(.list(.object(Series.selections)))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(series: [Series?]) {
      self.init(unsafeResultMap: ["__typename": "Query", "series": series.map { (value: Series?) -> ResultMap? in value.flatMap { (value: Series) -> ResultMap in value.resultMap } }])
    }

    public var series: [Series?] {
      get {
        return (resultMap["series"] as! [ResultMap?]).map { (value: ResultMap?) -> Series? in value.flatMap { (value: ResultMap) -> Series in Series(unsafeResultMap: value) } }
      }
      set {
        resultMap.updateValue(newValue.map { (value: Series?) -> ResultMap? in value.flatMap { (value: Series) -> ResultMap in value.resultMap } }, forKey: "series")
      }
    }

    public struct Series: GraphQLSelectionSet {
      public static let possibleTypes = ["Series"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .nonNull(.scalar(String.self))),
        GraphQLField("overview", type: .nonNull(.scalar(String.self))),
        GraphQLField("posterPath", type: .nonNull(.scalar(String.self))),
        GraphQLField("backdropPath", type: .nonNull(.scalar(String.self))),
        GraphQLField("status", type: .nonNull(.scalar(String.self))),
        GraphQLField("firstAirDate", type: .nonNull(.scalar(String.self))),
        GraphQLField("type", type: .nonNull(.scalar(String.self))),
        GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
        GraphQLField("seasons", type: .nonNull(.list(.object(Season.selections)))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(name: String, overview: String, posterPath: String, backdropPath: String, status: String, firstAirDate: String, type: String, uuid: String, seasons: [Season?]) {
        self.init(unsafeResultMap: ["__typename": "Series", "name": name, "overview": overview, "posterPath": posterPath, "backdropPath": backdropPath, "status": status, "firstAirDate": firstAirDate, "type": type, "uuid": uuid, "seasons": seasons.map { (value: Season?) -> ResultMap? in value.flatMap { (value: Season) -> ResultMap in value.resultMap } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var name: String {
        get {
          return resultMap["name"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }

      public var overview: String {
        get {
          return resultMap["overview"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "overview")
        }
      }

      public var posterPath: String {
        get {
          return resultMap["posterPath"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "posterPath")
        }
      }

      public var backdropPath: String {
        get {
          return resultMap["backdropPath"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "backdropPath")
        }
      }

      public var status: String {
        get {
          return resultMap["status"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "status")
        }
      }

      public var firstAirDate: String {
        get {
          return resultMap["firstAirDate"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "firstAirDate")
        }
      }

      public var type: String {
        get {
          return resultMap["type"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "type")
        }
      }

      public var uuid: String {
        get {
          return resultMap["uuid"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "uuid")
        }
      }

      public var seasons: [Season?] {
        get {
          return (resultMap["seasons"] as! [ResultMap?]).map { (value: ResultMap?) -> Season? in value.flatMap { (value: ResultMap) -> Season in Season(unsafeResultMap: value) } }
        }
        set {
          resultMap.updateValue(newValue.map { (value: Season?) -> ResultMap? in value.flatMap { (value: Season) -> ResultMap in value.resultMap } }, forKey: "seasons")
        }
      }

      public struct Season: GraphQLSelectionSet {
        public static let possibleTypes = ["Season"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .nonNull(.scalar(String.self))),
          GraphQLField("overview", type: .nonNull(.scalar(String.self))),
          GraphQLField("seasonNumber", type: .nonNull(.scalar(Int.self))),
          GraphQLField("airDate", type: .nonNull(.scalar(String.self))),
          GraphQLField("posterPath", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(uuid: String, name: String, overview: String, seasonNumber: Int, airDate: String, posterPath: String) {
          self.init(unsafeResultMap: ["__typename": "Season", "uuid": uuid, "name": name, "overview": overview, "seasonNumber": seasonNumber, "airDate": airDate, "posterPath": posterPath])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var uuid: String {
          get {
            return resultMap["uuid"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "uuid")
          }
        }

        public var name: String {
          get {
            return resultMap["name"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var overview: String {
          get {
            return resultMap["overview"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "overview")
          }
        }

        public var seasonNumber: Int {
          get {
            return resultMap["seasonNumber"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "seasonNumber")
          }
        }

        public var airDate: String {
          get {
            return resultMap["airDate"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "airDate")
          }
        }

        public var posterPath: String {
          get {
            return resultMap["posterPath"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "posterPath")
          }
        }
      }
    }
  }
}

public final class SeriesListQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition =
    """
    query SeriesList($offset: Int, $limit: Int) {
      series(offset: $offset, limit: $limit) {
        __typename
        posterPath
        name
        type
        uuid
        seasons {
          __typename
          name
          seasonNumber
          posterPath
          uuid
        }
      }
    }
    """

  public let operationName = "SeriesList"

  public var offset: Int?
  public var limit: Int?

  public init(offset: Int? = nil, limit: Int? = nil) {
    self.offset = offset
    self.limit = limit
  }

  public var variables: GraphQLMap? {
    return ["offset": offset, "limit": limit]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("series", arguments: ["offset": GraphQLVariable("offset"), "limit": GraphQLVariable("limit")], type: .nonNull(.list(.object(Series.selections)))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(series: [Series?]) {
      self.init(unsafeResultMap: ["__typename": "Query", "series": series.map { (value: Series?) -> ResultMap? in value.flatMap { (value: Series) -> ResultMap in value.resultMap } }])
    }

    public var series: [Series?] {
      get {
        return (resultMap["series"] as! [ResultMap?]).map { (value: ResultMap?) -> Series? in value.flatMap { (value: ResultMap) -> Series in Series(unsafeResultMap: value) } }
      }
      set {
        resultMap.updateValue(newValue.map { (value: Series?) -> ResultMap? in value.flatMap { (value: Series) -> ResultMap in value.resultMap } }, forKey: "series")
      }
    }

    public struct Series: GraphQLSelectionSet {
      public static let possibleTypes = ["Series"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("posterPath", type: .nonNull(.scalar(String.self))),
        GraphQLField("name", type: .nonNull(.scalar(String.self))),
        GraphQLField("type", type: .nonNull(.scalar(String.self))),
        GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
        GraphQLField("seasons", type: .nonNull(.list(.object(Season.selections)))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(posterPath: String, name: String, type: String, uuid: String, seasons: [Season?]) {
        self.init(unsafeResultMap: ["__typename": "Series", "posterPath": posterPath, "name": name, "type": type, "uuid": uuid, "seasons": seasons.map { (value: Season?) -> ResultMap? in value.flatMap { (value: Season) -> ResultMap in value.resultMap } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var posterPath: String {
        get {
          return resultMap["posterPath"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "posterPath")
        }
      }

      public var name: String {
        get {
          return resultMap["name"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }

      public var type: String {
        get {
          return resultMap["type"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "type")
        }
      }

      public var uuid: String {
        get {
          return resultMap["uuid"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "uuid")
        }
      }

      public var seasons: [Season?] {
        get {
          return (resultMap["seasons"] as! [ResultMap?]).map { (value: ResultMap?) -> Season? in value.flatMap { (value: ResultMap) -> Season in Season(unsafeResultMap: value) } }
        }
        set {
          resultMap.updateValue(newValue.map { (value: Season?) -> ResultMap? in value.flatMap { (value: Season) -> ResultMap in value.resultMap } }, forKey: "seasons")
        }
      }

      public struct Season: GraphQLSelectionSet {
        public static let possibleTypes = ["Season"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .nonNull(.scalar(String.self))),
          GraphQLField("seasonNumber", type: .nonNull(.scalar(Int.self))),
          GraphQLField("posterPath", type: .nonNull(.scalar(String.self))),
          GraphQLField("uuid", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(name: String, seasonNumber: Int, posterPath: String, uuid: String) {
          self.init(unsafeResultMap: ["__typename": "Season", "name": name, "seasonNumber": seasonNumber, "posterPath": posterPath, "uuid": uuid])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var name: String {
          get {
            return resultMap["name"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var seasonNumber: Int {
          get {
            return resultMap["seasonNumber"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "seasonNumber")
          }
        }

        public var posterPath: String {
          get {
            return resultMap["posterPath"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "posterPath")
          }
        }

        public var uuid: String {
          get {
            return resultMap["uuid"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "uuid")
          }
        }
      }
    }
  }
}
