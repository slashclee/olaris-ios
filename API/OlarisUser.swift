//
//  OlarisUser.swift
//  Olaris
//
//  Copyright (c) 2019 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import Foundation
import Valet

struct OlarisUser {
	private struct UserData: Codable {
		let password: String
		let server: String
	}

	private static let valet: Valet = ({ () in
		#if targetEnvironment(simulator)
		return Valet.valet(with: Identifier(nonEmpty: "tv.olaris.Olaris")!, accessibility: .whenUnlocked)
		#else
		return Valet.iCloudValet(with: Identifier(nonEmpty: "tv.olaris.Olaris")!, accessibility: .whenUnlocked)
		#endif
	})()

	public static var canAccessKeychain: Bool {
		valet.canAccessKeychain()
	}

	let username: String

	static var all: [OlarisUser] {
		valet.allKeys().map { OlarisUser(username: $0) }
	}

	private var userData: UserData? {
		guard let data = OlarisUser.valet.object(forKey: username) else {
			return nil
		}
		return try? JSONDecoder().decode(UserData.self, from: data)
	}

	var credentials: (username: String, password: String, server: String)? {
		guard let userData = userData else {
			return nil
		}
		return (username, userData.password, userData.server)
	}

	var password: String? {
		userData?.password
	}

	var server: String? {
		userData?.server
	}

	private init(username: String) {
		self.username = username
	}

	init?(username: String, password: String, server: String) {
		let userData = UserData(password: password, server: server)
		guard
			let data = try? JSONEncoder().encode(userData),
			OlarisUser.valet.set(object: data, forKey: username)
		else {
			return nil
		}
		self.username = username
	}

	func delete() {
		OlarisUser.valet.removeObject(forKey: username)
	}
}
