//  GraphQLOperationRunner.swift
//  Olaris
//
//  Copyright (c) 2019 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Apollo
import Combine
import Foundation

open class GraphQLOperationRunner<Data>: ObservableObject {
	typealias Status = DataProviderStatus<Data, Error>

	var data: Data? {
		switch status {
		case .unloaded:
			return nil
		case .loading(let data):
			return data
		case .loaded(let data):
			return data
		case .failed(let data, _):
			return data
		}
	}

	var failed: Error? {
		switch status {
		case .failed(_, let error):
			return error
		default:
			return nil
		}
	}

	public lazy var objectWillChange = ({
		statusSubject
			.share()
			.map { _ -> Void in return }
			.eraseToAnyPublisher()
	})()

	let client: ApolloClientProtocol
	let cachePolicy: CachePolicy
	let dispatchQueue: DispatchQueue

	private let statusSubject = PassthroughSubject<Status, Never>()
	private(set) var status: Status = .unloaded {
		didSet {
			statusSubject.send(self.status)
		}
	}

	init(client: ApolloClientProtocol, cachePolicy: CachePolicy = .returnCacheDataAndFetch, dispatchQueue: DispatchQueue = DispatchQueue(label: "GraphQLQueryRunner"), start: Bool = false) {
		self.client = client
		self.cachePolicy = cachePolicy
		self.dispatchQueue = dispatchQueue

		if start {
			self.start()
		}
	}

	var canRun: Bool {
		switch status {
		case .unloaded:
			return true
		default:
			return false
		}
	}

	func start() {
		guard canRun else { return }

		status = .unloaded
		run { status in self.status = status }
	}

	func run(update: @escaping (Status) -> Void) {
		// override this
	}
}
