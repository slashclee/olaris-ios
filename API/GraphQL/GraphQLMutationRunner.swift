//  GraphQLMutationRunner.swift
//  Olaris
//
//  Copyright (c) 2019 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Apollo
import Combine
import Foundation

class GraphQLMutationRunner<Mutation: GraphQLMutation>: GraphQLOperationRunner<Mutation.Data> {
	enum MutationError: Error {
		case multipleErrors([Error])
	}

	let mutation: Mutation
	let context: UnsafeMutableRawPointer?
	private var cancellable: Apollo.Cancellable? = nil

	init(mutation: Mutation, context: UnsafeMutableRawPointer? = nil, client: ApolloClientProtocol, cachePolicy: CachePolicy = .returnCacheDataAndFetch, dispatchQueue: DispatchQueue = DispatchQueue(label: "GraphQLQueryRunner"), start: Bool = false) {
		self.mutation = mutation
		self.context = context
		super.init(client: client, cachePolicy: cachePolicy, dispatchQueue: dispatchQueue, start: start)
	}

	override func run(update: @escaping (Status) -> Void) {
		super.run(update: update)
		update(.loading(nil))
		cancellable = client.perform(mutation: mutation, context: context, queue: dispatchQueue) { (result: Result<GraphQLResult<Mutation.Data>, Error>) in
			switch result {
			case .success(let queryResult):
				if let data = queryResult.data {
					update(.loaded(data))
				}
				else if let errors = queryResult.errors {
					update(.failed(nil, MutationError.multipleErrors(errors) as Error))
				}
			case .failure(let error):
				update(.failed(nil, error))
			}
		}
	}
}
