//  GraphQLQueryRunner.swift
//  Olaris
//
//  Copyright (c) 2019 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Apollo
import Combine
import Foundation

class GraphQLQueryRunner<Query: GraphQLQuery>: GraphQLOperationRunner<Query.Data> {
	let query: Query
	private var queryWatcher: GraphQLQueryWatcher<Query>? = nil

	init(query: Query, client: ApolloClientProtocol, cachePolicy: CachePolicy = .returnCacheDataAndFetch, dispatchQueue: DispatchQueue = DispatchQueue(label: "GraphQLQueryRunner"), start: Bool = false) {
		self.query = query
		super.init(client: client, cachePolicy: cachePolicy, dispatchQueue: dispatchQueue, start: start)
	}

	override func run(update: @escaping (Status) -> Void) {
		super.run(update: update)
		update(.loading(nil))
		queryWatcher = client.watch(query: query, cachePolicy: cachePolicy, queue: dispatchQueue) { (result: Result<GraphQLResult<Query.Data>, Error>) in
			switch result {
			case .success(let queryResult):
				if let data = queryResult.data {
					update(.loaded(data))
				}
				else {
					assertionFailure()
				}
			case .failure(let error):
				update(.failed(nil, error))
			}
		}
	}
}
