//
//  GraphQLClient.swift
//  Olaris
//
//  Copyright (c) 2019 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Apollo
import Combine
import Foundation
import SwiftUI

class GraphQLClient: ObservableObject {
    private class ApolloHTTPDelegate: HTTPNetworkTransportPreflightDelegate {
		let jwt: String

		init(jwt: String) {
			self.jwt = jwt
		}

        func networkTransport(_ networkTransport: HTTPNetworkTransport, shouldSend request: URLRequest) -> Bool {
            return true
        }

        func networkTransport(_ networkTransport: HTTPNetworkTransport, willSend request: inout URLRequest) {
            request.setValue("Bearer \(jwt)", forHTTPHeaderField: "Authorization")
        }
    }

	private let apolloDelegate: ApolloHTTPDelegate
    let apolloClient: ApolloClient

    private let objectWillChangeSubject: PassthroughSubject<ApolloClient, Never>
    let objectWillChange: AnyPublisher<ApolloClient, Never>

	let server: OlarisServer
	let jwt: String
	let graphqlURL: URL

	init?(server: OlarisServer, jwt: String) {
		guard let graphqlURL = server.graphqlURL, !jwt.isEmpty else {
			return nil
		}

		self.server = server
		self.jwt = jwt
		self.graphqlURL = graphqlURL

		self.apolloDelegate = ApolloHTTPDelegate(jwt: jwt)

        let objectWillChangeSubject = PassthroughSubject<ApolloClient, Never>()
        let objectWillChangePublisher = objectWillChangeSubject.eraseToAnyPublisher()
        self.objectWillChangeSubject = objectWillChangeSubject
        self.objectWillChange = objectWillChangePublisher

        let session = URLSession(configuration: .default)
        let transport = HTTPNetworkTransport(
			url: graphqlURL,
            session: session,
            sendOperationIdentifiers: false,
            useGETForQueries: false,
            enableAutoPersistedQueries: false,
            useGETForPersistedQueryRetry: false,
            delegate: apolloDelegate,
            requestCreator: ApolloRequestCreator()
        )
        let cache = InMemoryNormalizedCache()
        let store = ApolloStore(cache: cache)

        let client = ApolloClient(networkTransport: transport, store: store)
        self.apolloClient = client

        defer { objectWillChangeSubject.send(client) }
    }

	func imageURL(path: String, original: Bool = false) -> URL? {
		URL(string: "/olaris/m/images/tmdb/\(original ? "original" : "w342")\(path)", relativeTo: server.baseURL)
    }
}
