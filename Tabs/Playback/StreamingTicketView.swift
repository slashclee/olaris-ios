//  StreamingTicketView.swift
//  Olaris
//
//  Copyright (c) 2019 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import SwiftUI

#if !os(macOS) && !os(tvOS)
import VideoPlayer
#endif

struct StreamingTicketView: View {
	@State var playable: Playable
	@ObservedObject var mutationRunner: 	GraphQLMutationRunner<CreateStreamingTicketMutation>
	@EnvironmentObject var olarisServerController: OlarisServerController

	var hlsURL: URL? {
		guard
			let hlsPath = mutationRunner.data?.createStreamingTicket.hlsStreamingPath,
			let hlsURL = olarisServerController.url(path: hlsPath)
		else {
			return nil
		}

		return hlsURL
	}

	var body: some View {
		return Group {
			Text(playable.name)
			#if !os(macOS) && !os(tvOS)
			hlsURL.map { url in
				Player(videoURL: url)
					.frame(maxWidth: .infinity)
			}
			#endif
		}
			.onAppear { self.mutationRunner.start() }
	}
}

//struct StreamingTicketView_Previews: PreviewProvider {
//    static var previews: some View {
//        StreamingTicketView()
//    }
//}
