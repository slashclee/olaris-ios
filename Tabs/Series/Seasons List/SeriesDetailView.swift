//  SeriesDetailView.swift
//  Olaris
//
//  Copyright (c) 2019 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Apollo
import Combine
import SwiftUI
import URLImage

struct SeriesDetailView: View {
	@ObservedObject var queryRunner: GraphQLQueryRunner<SeriesDetailQuery>
	@EnvironmentObject var graphqlClient: GraphQLClient

	var series: SeriesDetailQuery.Data.Series? {
		queryRunner.data?
			.series
			.compactMap { $0 }
			.first
	}

	var seasons: [SeriesDetailQuery.Data.Series.Season] {
		series?.seasons
			.compactMap { $0 }
			.sorted(by: \.seasonNumber)
		?? []
	}

	var subheadline: String {
		guard let series = self.series else {
			return ""
		}
		let components: [String] = [series.firstAirDate]
		return components.joined(separator: " • ")
	}

	var backgroundView: some View {
		let context = CIContext(options: nil)
		return series.flatMap { graphqlClient.imageURL(path: $0.backdropPath) }.flatMap { imageURL in
			URLImage(imageURL, processors: [
				CoreImageFilterProcessor(name: "CIGaussianBlur", parameters: [kCIInputRadiusKey: 3], context: context),
				CoreImageFilterProcessor(name: "CIVibrance", parameters: [kCIInputAmountKey: 20], context: context),
				], content: {
					$0.image
						.renderingMode(.original)
						.resizable()
						.aspectRatio(contentMode: .fill)
						.clipped()
						.opacity(0.5)
			})
		}
	}

	func scrollView(series: SeriesDetailQuery.Data.Series) -> some View {
		let scrollView = ScrollView(.vertical, showsIndicators: true) {
			VStack {
				Text(series.name)
					.font(.title)
					.foregroundColor(.primary)
					.frame(maxWidth: .infinity, alignment: .leading)
					.padding(.top, 20)
				Text(self.subheadline + "\n")
					.font(.subheadline)
					.foregroundColor(.secondary)
					.frame(maxWidth: .infinity, alignment: .leading)
				Text(series.overview + "\n")
					.font(.body)
					.foregroundColor(.primary)
					.frame(maxWidth: .infinity, alignment: .leading)
					.multilineTextAlignment(.leading)
			}
			.padding(EdgeInsets(top: 0, leading: 15, bottom: 0, trailing: 15))
			SeasonsGrid(seasons: self.seasons, minimumCellWidth: 120)
				.padding(.bottom, 20)
				.frame(minHeight: 900)

		}
		.background(backgroundView)
		.frame(maxWidth: .infinity, maxHeight: .infinity)
		#if os(iOS)
		return scrollView
			.navigationBarTitle("\(series.name)", displayMode: .inline)
		#else
		return scrollView
		#endif	
	}

	var body: some View {
		series.map { series in
			scrollView(series: series)
		}
		.onAppear { self.queryRunner.start() }
	}
}

extension SeriesDetailQuery.Data.Series.Season: Identifiable {
	public typealias ID = String
	public var id: String {
		return uuid
	}
}
