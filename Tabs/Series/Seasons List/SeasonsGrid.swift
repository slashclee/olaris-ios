//  SeriesDetailGrid.swift
//  Olaris
//
//  Copyright (c) 2019 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import GridStack
import SwiftUI

struct SeasonsGrid: View {
	@EnvironmentObject var graphqlClient: GraphQLClient
	var seasons: [SeriesDetailQuery.Data.Series.Season]
	var minimumCellWidth: CGFloat

	func cell(index: Int, width: CGFloat) -> some View {
		let season = seasons[index]
		#if os(tvOS)
		let cell = SeasonGridCell(season: season)
			.focusable()
		#else
		let cell = SeasonGridCell(season: season)
		#endif

		return cell
//
//		return NavigationLink(destination: MovieDetailView(queryRunner: GraphQLQueryRunner(query: MovieDetailQuery(uuid: movie.uuid), client: graphqlClient.apolloClient))) {
//			cell
//		}
	}

	var grid: some View {
		GridStack(minCellWidth: self.minimumCellWidth, spacing: 8, numItems: seasons.count, alignment: .center, content: { (index, width) in
			self.cell(index: index, width: width)
		})
	}

	var body: some View {
		grid
	}
}
