//  MovieDetailView.swift
//  Olaris
//
//  Copyright (c) 2019 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Apollo
import Combine
import SwiftUI
import URLImage

struct MovieDetailView: View {
	@ObservedObject var queryRunner: GraphQLQueryRunner<MovieDetailQuery>
	@EnvironmentObject var graphqlClient: GraphQLClient

	var movie: MovieDetailQuery.Data.Movie? {
		queryRunner.data?
			.movie
			.compactMap { $0 }
			.first
	}

	var file: MovieDetailQuery.Data.Movie.File? {
		movie?.files.compactMap({ $0 }).first
	}

	var duration: String? {
		guard let duration = self.file?.totalDuration else {
			return nil
		}
		let hours = floor(duration / 3600)
		let minutes = floor((duration - (hours * 3600)) / 60)
		let seconds = floor((duration - (hours * 3600) - (minutes * 60)) / 1)

		var components: [String] = []
		if hours > 0 {
			components.append("\(Int(hours))h")
		}
		if hours > 0 || minutes > 0 {
			components.append("\(Int(minutes))m")
		}
		if hours == 0 {
			components.append("\(Int(seconds))s")
		}
		return components.joined(separator: "")
	}

	var subheadline: String {
		guard let movie = self.movie else {
			return ""
		}
		var components: [String] = [movie.year]
		if let duration = self.duration {
			components.append("\(duration)")
		}
		if let playState = movie.playState {
			components.append("\(playState.finished ? "Finished" : "Unfinished")")
		}
		return components.joined(separator: " • ")
	}

	var backgroundView: some View {
		let context = CIContext(options: nil)
		return movie.flatMap { graphqlClient.imageURL(path: $0.backdropPath) }.flatMap { imageURL in
			URLImage(imageURL, processors: [
				CoreImageFilterProcessor(name: "CIGaussianBlur", parameters: [kCIInputRadiusKey: 3], context: context),
				CoreImageFilterProcessor(name: "CIVibrance", parameters: [kCIInputAmountKey: 20], context: context),
				], content: {
					$0.image
						.renderingMode(.original)
						.resizable()
						.aspectRatio(contentMode: .fill)
						.clipped()
						.opacity(0.5)
			})
		}
	}

	func scrollView(movie: MovieDetailQuery.Data.Movie) -> some View {
		let scrollView = ScrollView(.vertical, showsIndicators: true) {
			HStack(alignment: .top) {
				VStack {
					Text(movie.name)
						.font(.title)
						.foregroundColor(.primary)
						.frame(maxWidth: .infinity, alignment: .leading)
					Text(self.subheadline + "\n")
						.font(.subheadline)
						.foregroundColor(.secondary)
						.frame(maxWidth: .infinity, alignment: .leading)
					Text(movie.overview + "\n\n")
						.font(.body)
						.foregroundColor(.primary)
						.frame(maxWidth: .infinity, alignment: .leading)
						.multilineTextAlignment(.leading)
					file.map { file in
						NavigationLink(destination: StreamingTicketView(playable: movie, mutationRunner: GraphQLMutationRunner(mutation: CreateStreamingTicketMutation(uuid: file.uuid), client: self.graphqlClient.apolloClient))) {
							Text("Play")
								.font(.callout)
								.fontWeight(.bold)
								.foregroundColor(.accentColor)
						}
					}
				}
			}
			.padding(EdgeInsets(top: 66, leading: 20, bottom: 20, trailing: 20))
			.frame(maxWidth: .infinity, maxHeight: .infinity)
		}
		.background(backgroundView)


		#if os(iOS)
		return scrollView
			.navigationBarTitle("\(movie.name)", displayMode: .inline)
		#else
		return scrollView
		#endif
	}

	var body: some View {
		movie.map { movie in
			scrollView(movie: movie)
		}
		.onAppear { self.queryRunner.start() }
	}
}

extension MovieDetailQuery.Data.Movie: Playable {
}

extension MovieDetailQuery.Data.Movie.File: Identifiable {
	public typealias ID = String
	public var id: String {
		return uuid
	}
}
