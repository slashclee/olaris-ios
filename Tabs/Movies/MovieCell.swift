//  MovieCell.swift
//  Olaris
//
//  Copyright (c) 2019 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import SwiftUI
import URLImage

struct MovieCell: View {
	@EnvironmentObject var graphqlClient: GraphQLClient
	var movie: MovieListQuery.Data.Movie

    var body: some View {
		graphqlClient.imageURL(path: movie.posterPath).map { imageURL in
			URLImage(imageURL, content: {
				$0.image
					.renderingMode(.original)
					.resizable()
					.aspectRatio(contentMode: .fill)
					.clipped()
			})
		}
    }
}

//struct MovieCell_Previews: PreviewProvider {
//    static var previews: some View {
//		MovieCell(movie: MovieListQuery.Data.Movie(name: "Foo", posterPath: <#T##String#>, uuid: <#T##String#>, year: <#T##String#>, files: <#T##[MovieListQuery.Data.Movie.File?]#>))
//    }
//	}
