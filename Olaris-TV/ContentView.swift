//
//  ContentView.swift
//  Olaris-TV
//
//  Copyright (c) 2019 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var serverController: OlarisServerController
    
    var body: some View {
        TabView {
            Text("Dashboard")
                .tabItem {
                    Image(systemName: "gauge").font(.headline)
                    Text("Dashboard")
                }
			self.serverController.graphqlClient.map { graphqlClient in
				MoviesTab(
					queryRunner: GraphQLQueryRunner(query: MovieListQuery(), client: graphqlClient.apolloClient, start: true)
					, minimumCellWidth: 200)
					.tabItem {
						Image(systemName: "film").font(.headline)
						Text("Movies")
					}
			}
            Text("TV Shows")
                .tabItem {
                    Image(systemName: "tv").font(.headline)
                    Text("TV Shows")
                }
        }
        .font(.headline)
		.sheet(isPresented: Binding(get: { self.serverController.isLoggedOut }, set: { _ in })) { () in
			LoginSheet()
				.environmentObject(self.serverController)
		}
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
