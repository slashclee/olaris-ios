//  EpisodesTableViewController.swift
//  Olaris-iOS
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import Foundation
import SDWebImage
import UIKit

private let reuseIdentifier = "EpisodeTableViewCell"
final class EpisodesTableViewController: UITableViewController {
	let serverController: OlarisServerController
	let series: SeriesListQuery.Data.Series
	let season: SeriesListQuery.Data.Series.Season

	var tableViewBinding: TableViewBinding<HashableSectionProvider<DataProvidingSectionProvider<AnyDataProvider<[EpisodesForSeasonQuery.Data.Season.Episode], Error>>, String>>?
	var tableViewDelegate: UITableViewDelegate? = nil {
		didSet {
			tableView.delegate = tableViewDelegate
		}
	}

	var headerView: TOHeaderImageView? = nil

	var selectItem: AnyCancellable? = nil

	init(series: SeriesListQuery.Data.Series, season: SeriesListQuery.Data.Series.Season, serverController: OlarisServerController) {
		self.series = series
		self.season = season
		self.serverController = serverController

		super.init(style: .plain)

		self.title = "\(series.name) \(season.name)"
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		self.tableView?.backgroundColor = OlarisAssets.background

		// Uncomment the following line to preserve selection between presentations
		// self.clearsSelectionOnViewWillAppear = false

//		let manager = SDWebImageManager.shared
//		if
//			let imageURL = serverController.graphqlClient?.imageURL(path: season.posterPath),
//			let imageCacheKey = manager.cacheKey(for: imageURL)
//		{
//			let _ = manager.imageCache.queryImage(forKey: imageCacheKey, options: .fromCacheOnly, context: nil) { (image, _, _) in
//				guard Thread.isMainThread, let image = image, let tableView = self.tableView else {
//					return
//				}
//
//				let height = tableView.frame.size.width * image.size.height / image.size.width
//				let scale = tableView.window?.screen.scale ?? 1
//				let roundedHeight = round(height * scale) / scale
//
//				let header = TOHeaderImageView(image: image, height: roundedHeight)
//				self.headerView = header
//				tableView.tableHeaderView = header
//			}
//		}
//		let fullImageURL = serverController.graphqlClient?.imageURL(path: season.posterPath)
//
//		manager.imageCache.queryImage(forKey: manager., options: <#T##SDWebImageOptions#>, context: <#T##[SDWebImageContextOption : Any]?#>, completion: <#T##SDImageCacheQueryCompletionBlock?##SDImageCacheQueryCompletionBlock?##(UIImage?, Data?, SDImageCacheType) -> Void#>)

		guard let graphqlClient = serverController.graphqlClient else {
			assertionFailure("Need a GraphQL Client here")
			return
		}

		// Register cell classes
		self.tableView?.register(UINib(nibName: "EpisodeTableViewCell", bundle: nil), forCellReuseIdentifier: reuseIdentifier)

		// Do any additional setup after loading the view.

		let sectionProvider = EpisodesForSeasonQuery(seasonUUID: season.uuid)
			.nonOptionalArrayDataProvider(serverController: serverController)
			.deduplicated(by: \.uuid)
			.sorted(by: \.airDate)
			.asAnyDataProvider
			.sectionProvider(hashing: \.uuid)

		let tableViewBinding = TableViewBinding(
			tableView: self.tableView,
			sectionProvider: sectionProvider,
			canRefresh: true,
			rowAnimation: .fade,
			cellProvider: { (episode, indexPath, tableView) -> UITableViewCell in
				let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
				guard let episodeCell = cell as? EpisodeTableViewCell else {
					assertionFailure()
					return cell
				}
				episodeCell.graphqlClient = self.serverController.graphqlClient
				episodeCell.episode = episode.wrapped
				return episodeCell
			}
		)
		self.tableViewBinding = tableViewBinding

		let delegate = TableViewPublisherDelegate(tableView: tableView)
		tableViewDelegate = delegate

		selectItem = delegate.selected
			.filter({ (indexPath, selected) -> Bool in
				guard case .selected = selected else {
					return false
				}
				return true
			})
			.map { (indexPath, _) -> IndexPath in indexPath }
			.map { indexPath -> EpisodesForSeasonQuery.Data.Season.Episode? in
				tableViewBinding.sectionProvider.item(at: indexPath, full: indexPath)?.wrapped
		}
		.filteringOptionals
		.map({ episode in episode.files.first })
		.filteringOptionals
		.filteringOptionals
		.receive(on: DispatchQueue.main)
		.setFailureType(to: VideoPlayerViewController.CreateVideoPlayerError.self)
		.flatMap { file in
			VideoPlayerViewController.create(uuid: file.uuid, serverController: self.serverController, graphqlClient: graphqlClient)
		}
		.receive(on: DispatchQueue.main)
		.sink(receiveCompletion: { _ in }, receiveValue: { playerViewController in
			self.present(playerViewController, animated: true, completion: { () in })
		})

		sectionProvider.updateNewerIfNeeded()
	}

	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()

		if let tableView = self.tableView {
			let leftInset = tableView.frame.size.width * 0.25 + 15 + 16
			tableView.separatorInset = UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: 0)
		}
	}

	override func scrollViewDidScroll(_ scrollView: UIScrollView) {
		headerView?.scrollOffset = scrollView.contentOffset.y
	}

	#if os(iOS)
	override var preferredStatusBarStyle: UIStatusBarStyle {
		.lightContent
	}
	#endif
}
