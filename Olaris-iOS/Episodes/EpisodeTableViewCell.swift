//  EpisodeTableViewCell.swift
//  Olaris-iOS
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import SDWebImage
import UIKit

class EpisodeTableViewCell: UITableViewCell {

	@IBOutlet weak var previewImageView: UIImageView?

	@IBOutlet weak var titleLabel: UILabel?
	@IBOutlet weak var overviewLabel: UILabel?
	@IBOutlet weak var metadataLabel: UILabel?

	@IBOutlet weak var syncButton: UIButton?

	@IBAction func toggleSync(_ sender: Any) {
	}

	var graphqlClient: GraphQLClient?

	var episode: EpisodesForSeasonQuery.Data.Season.Episode? {
		didSet {
			guard let episode = self.episode else {
				return
			}

			titleLabel?.text = "\(episode.episodeNumber). \(episode.name)"
			overviewLabel?.text = episode.overview
			let dateFormatter = DateFormatter()
			dateFormatter.dateFormat = "YYYY-MM-DD"

			var fields: [String] = []

			if let airDate = dateFormatter.date(from: episode.airDate) {
				dateFormatter.dateStyle = .medium
				dateFormatter.timeStyle = .none
				fields.append(dateFormatter.string(from: airDate))
			}

			if let duration = episode.files.reduce(nil, { (initial, file) -> Double? in
				initial ?? file?.totalDuration
			}) {
				let numberOfMinutes = Int(round(duration / 60))
				let minutes = NSString.localizedStringWithFormat("%zd mins", numberOfMinutes) as String
				fields.append(minutes)
			}

			metadataLabel?.text = fields.joined(separator: " • ")

			if let imageURL = graphqlClient?.imageURL(path: episode.stillPath) {
				previewImageView?.sd_setImage(with: imageURL)
			}
		}
	}

	override func awakeFromNib() {
		super.awakeFromNib()

		previewImageView?.layer.cornerRadius = 4
		previewImageView?.clipsToBounds = true
		previewImageView?.layer.masksToBounds = true
	}
}
