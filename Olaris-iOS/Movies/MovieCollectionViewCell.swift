//  MovieCollectionViewCell.swift
//  Olaris-iOS
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import SDWebImage
import UIKit

#if os(tvOS)
import TVUIKit
#endif

class MovieCollectionViewCell: UICollectionViewCell {
	#if os(iOS)
	@IBOutlet weak var posterView: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!
	#elseif os(tvOS)
	@IBOutlet weak var posterView: TVPosterView!
	#endif

	var graphqlClient: GraphQLClient?

	var movie: MovieListQuery.Data.Movie? {
		didSet {
			#if os(iOS)
			nameLabel?.text = movie?.name
			if
				let posterPath = movie?.posterPath,
				let imageURL = graphqlClient?.imageURL(path: posterPath)
			{
				posterView?.sd_setImage(with: imageURL)
			}
			#elseif os(tvOS)
			posterView?.title = movie?.name
			posterView?.subtitle = nil
			if
				let posterPath = movie?.posterPath,
				let imageURL = graphqlClient?.imageURL(path: posterPath)
			{
				posterView?.imageView.sd_setImage(with: imageURL)
			}
			#endif
		}
	}
}
