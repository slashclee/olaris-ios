//  MovieCollectionViewController.swift
//  Olaris-iOS
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Apollo
import AVFoundation
import AVKit
import Combine
import CoreSpotlight
import UIKit

class MovieCollectionViewController: UICollectionViewController, AVPlayerViewControllerDelegate, AVAssetResourceLoaderDelegate, UICollectionViewDelegateFlowLayout {

	enum CellType: Hashable {
		case header(MovieHeaderCell.ViewModel)
		case description(String)

		var cellIdentifier: String {
			switch self {
			case .header(_):
				return "header"
			case .description(_):
				return "description"
			}
		}

		func hash(into hasher: inout Hasher) {
			hasher.combine(cellIdentifier)

			switch self {
			case .header(let value):
				hasher.combine(value.uuid)
				hasher.combine(value.name)
				hasher.combine(value.imagePath)
			case .description(let text):
				hasher.combine(text)
			}
		}

		static func == (lhs: MovieCollectionViewController.CellType, rhs: MovieCollectionViewController.CellType) -> Bool {
			guard lhs.cellIdentifier == rhs.cellIdentifier else {
				return false
			}

			var lhsHasher = Hasher()
			var rhsHasher = Hasher()

			lhsHasher.combine(lhs)
			rhsHasher.combine(rhs)

			let lhsHash = lhsHasher.finalize()
			let rhsHash = rhsHasher.finalize()

			return lhsHash == rhsHash
		}
	}

	let expandedAppearance = OlarisAssets.largeTitleNavigationAppearance
	let compactAppearance = OlarisAssets.standardNavigationAppearance

	let inputMovie: MovieListQuery.Data.Movie
	let serverController: OlarisServerController
	private(set) var collectionViewBinding: CollectionViewBinding<StaticSectionDataProvider<InitialAndFetchedDataProvider<MovieListQuery.Data.Movie, GraphQLSelectionSetDataProvider<MovieDetailQuery>>, MovieCollectionViewController.CellType>>? = nil

	init(movie: MovieListQuery.Data.Movie, serverController: OlarisServerController) {
		self.inputMovie = movie
		self.serverController = serverController

		let layout = UICollectionViewFlowLayout()

		super.init(collectionViewLayout: layout)

		title = movie.name

		let dataProvider = MovieDetailQuery(uuid: movie.uuid)
			.dataProvider(serverController: serverController)
			.withSelectionSet
			.withInitialValue(value: movie)

		let sectionProvider = StaticSectionDataProvider(provider: dataProvider) { value -> [[CellType]] in
			guard let data = value.data else {
				return []
			}

			switch data {
			case .initial(let movie):
				let file = movie.files.compactMap({ file in file }).first

				let headerViewModel = MovieHeaderCell.ViewModel(uuid: file?.uuid, name: movie.name, imagePath: movie.posterPath)
				return [
					[
						.header(headerViewModel),
						.description(movie.overview)
					]
				]
			case .fetched(let fetched):
				guard let fetchedMovie = fetched else {
					return []
				}
				guard let file = fetchedMovie.files.compactMap({ file in file }).first else {
					return []
				}

				let headerViewModel = MovieHeaderCell.ViewModel(uuid: file.uuid, name: fetchedMovie.name, imagePath: fetchedMovie.posterPath)
				return [
					[
						.header(headerViewModel),
						.description(movie.overview)
					]
				]
			}
		}

		self.collectionViewBinding = CollectionViewBinding(
			collectionView: self.collectionView!,
			sectionProvider: sectionProvider,
			canRefresh: false,
			cellProvider: { (value, indexPath, collectionView) -> UICollectionViewCell in
				let cell = collectionView.dequeueReusableCell(withReuseIdentifier: value.cellIdentifier, for: indexPath)
				if case .header(let viewModel) = value, let headerCell = cell as? MovieHeaderCell {
					headerCell.update(viewModel: viewModel, serverController: self.serverController) { uuid in
						self.play(uuid: uuid)
					}
				}
				else if case .description(let text) = value, let descriptionCell = cell as? DescriptionCollectionViewCell {
					descriptionCell.update(text: text)
				}
				return cell
			}
		)
	}

	private var cancellable: AnyCancellable? = nil
	func play(uuid: String) {
		guard let graphqlClient = serverController.graphqlClient else {
			assertionFailure()
			return
		}

		cancellable = VideoPlayerViewController.create(uuid: uuid, serverController: self.serverController, graphqlClient: graphqlClient)
			.receive(on: DispatchQueue.main)
			.sink(receiveCompletion: { _ in }, receiveValue: { playerViewController in
				self.present(playerViewController, animated: true, completion: { () in })
			})
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	lazy var activity: NSUserActivity = ({
		let activity = NSUserActivity(activityType: "OpenMovieIntent")
		activity.title = inputMovie.name
		activity.isEligibleForSearch = true
		activity.isEligibleForHandoff = true
		activity.isEligibleForPublicIndexing = false
		activity.isEligibleForPrediction = false
		activity.keywords = Set(["movie", "movies", "olaris", inputMovie.name])
		return activity
	})()

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		activity.becomeCurrent()
	}

    override func viewDidLoad() {
        super.viewDidLoad()

		guard let collectionView = self.collectionView else {
			fatalError()
		}

		collectionView.backgroundColor = OlarisAssets.background

//		navigationItem.scrollEdgeAppearance = expandedAppearance
//		navigationItem.standardAppearance = compactAppearance

//		navigationItem. TitleDisplayMode = .never

		collectionView.register(MovieHeaderCell.nib, forCellWithReuseIdentifier: "header")
		collectionView.register(DescriptionCollectionViewCell.nib, forCellWithReuseIdentifier: "description")

        // Do any additional setup after loading the view.
		let layout = collectionView.collectionViewLayout
		if let flowLayout = layout as? UICollectionViewFlowLayout {
			flowLayout.estimatedItemSize = CGSize(
				width: collectionView.widestCellWidth,
				// Make the height a reasonable estimate to
				// ensure the scroll bar remains smooth
				height: 200
			)
		}
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		collectionViewBinding?.sectionProvider.updateNewerIfNeeded()
	}	

	#if os(iOS)
	override var preferredStatusBarStyle: UIStatusBarStyle {
		.lightContent
	}
	#endif
}
