//  MovieModalViewController.swift
//  Olaris-iOS
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Apollo
import AVFoundation
import AVKit
import Combine
import SDWebImage
import UIKit

struct StreamingTicketMetadata: Codable {
	let checkCodecs: [String]
}

class MovieModalViewController: UIViewController, AVPlayerViewControllerDelegate, AVAssetResourceLoaderDelegate {

	@IBOutlet weak var posterImageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var playButton: UIButton!

	let movie: MovieListQuery.Data.Movie
	let serverController: OlarisServerController

	init(movie: MovieListQuery.Data.Movie, serverController: OlarisServerController) {
		self.movie = movie
		self.serverController = serverController

		super.init(nibName: "MovieModalViewController", bundle: nil)
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		posterImageView.backgroundColor = OlarisAssets.background

		titleLabel.text = movie.name
		descriptionLabel.text = movie.overview

		playButton?.layer.cornerRadius = 4
		playButton?.clipsToBounds = true
		playButton?.backgroundColor = .systemPurple

		if
			let posterImageView = self.posterImageView,
			let graphqlClient = serverController.graphqlClient,
			let imageURL = graphqlClient.imageURL(path: movie.posterPath, original: false)
		{
			let fullSizePosterImageURL = graphqlClient.imageURL(path: movie.posterPath, original: true)
			posterImageView.sd_setImage(with: imageURL, completed: { (image, _, _, _) in
				DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
					if let posterImageURL = fullSizePosterImageURL {
						posterImageView.sd_setImage(with: posterImageURL, placeholderImage: image, options: SDWebImageOptions(), completed: { (fullImage, a, b, c) in
							posterImageView.image = fullImage ?? image
						})
					}
				}
			})
		}

		let navAppearance = UINavigationBarAppearance()
		navAppearance.configureWithTransparentBackground()
		navigationItem.standardAppearance = navAppearance
    }

	/*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

	var cancellable: Combine.Cancellable? = nil

	@IBAction func play(_ sender: Any) {
		guard
			let first = movie.files.first,
			let file = first,
			let graphqlClient = serverController.graphqlClient
		else {
			return
		}

		let create = VideoPlayerViewController.create(uuid: file.uuid, serverController: serverController, graphqlClient: graphqlClient, assetDelegate: self)
		cancellable = create
			.sink(receiveCompletion: { _ in }, receiveValue: { playerViewController in
				self.present(playerViewController, animated: true, completion: { () in })
			})
	}

	#if os(iOS)
	override var preferredStatusBarStyle: UIStatusBarStyle {
		.lightContent
	}
	#endif
}
