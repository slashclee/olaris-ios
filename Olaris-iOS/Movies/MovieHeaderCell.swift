//  MovieHeaderCell.swift
//  Olaris-iOS
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import SDWebImage
import UIKit

class MovieHeaderCell: UICollectionViewCell {
	struct ViewModel: Hashable {
		let uuid: String?
		let name: String
		let imagePath: String

		init(uuid: String?, name: String, imagePath: String) {
			self.uuid = uuid
			self.name = name
			self.imagePath = imagePath
		}

		func hash(into hasher: inout Hasher) {
			hasher.combine(uuid)
			hasher.combine(name)
			hasher.combine(imagePath)
		}
	}

	static let nib = UINib(nibName: "MovieHeaderCell", bundle: nil)
	static func fromNib() -> MovieHeaderCell {
		nib.instantiate(withOwner: nil, options: nil)[0] as! MovieHeaderCell
	}

	@IBOutlet weak var posterImageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var playButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

		posterImageView.backgroundColor = OlarisAssets.background

		playButton?.layer.cornerRadius = 4
		playButton?.clipsToBounds = true
		playButton?.backgroundColor = .systemPurple
    }

	private var handleTap: ((String) -> Void)?
	private var uuid: String? = nil
	func update(viewModel: ViewModel, serverController: OlarisServerController, handleTap: @escaping (String) -> Void) {
		self.uuid = viewModel.uuid
		self.handleTap = handleTap
		titleLabel.text = viewModel.name
		self.playButton.isHidden = (viewModel.uuid == nil)

		if let imageURL = serverController.graphqlClient?.imageURL(path: viewModel.imagePath, original: false), let posterView = self.posterImageView {
			posterView.sd_setImage(with: imageURL, placeholderImage: nil, options: .fromCacheOnly) { (image, _, _, _) in
				if let fullImageURL = serverController.graphqlClient?.imageURL(path: viewModel.imagePath, original: true) {
					SDWebImageManager.shared.loadImage(with: fullImageURL, options: SDWebImageOptions(), progress: nil) { (fullImage, _, _, _, _, _) in
						UIView.transition(with: posterView, duration: 0.3, options: .transitionCrossDissolve, animations: { () -> Void in
							posterView.image = fullImage
						})
					}
				}
			}
		}
	}

	@IBAction func play(_ sender: Any) {
		if let uuid = self.uuid {
			handleTap?(uuid)
		}
	}

	override func systemLayoutSizeFitting(
		_ targetSize: CGSize,
		withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority,
		verticalFittingPriority: UILayoutPriority) -> CGSize {

		// Replace the height in the target size to
		// allow the cell to flexibly compute its height
		var targetSize = targetSize
		targetSize.height = CGFloat.greatestFiniteMagnitude

		// The .required horizontal fitting priority means
		// the desired cell width (targetSize.width) will be
		// preserved. However, the vertical fitting priority is
		// .fittingSizeLevel meaning the cell will find the
		// height that best fits the content
		let size = super.systemLayoutSizeFitting(
			targetSize,
			withHorizontalFittingPriority: .required,
			verticalFittingPriority: .fittingSizeLevel
		)

		return size
	}
}
