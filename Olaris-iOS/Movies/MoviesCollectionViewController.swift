//  MoviesCollectionViewController.swift
//  Olaris-iOS
//
//  Copyright (c) 2019 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
#if os(iOS)
import Intents
#endif
import UIKit
#if os(tvOS)
import TVUIKit
#endif

private let reuseIdentifier = "Cell"

class MoviesCollectionViewController: UICollectionViewController {
	let serverController: OlarisServerController
	let layout: UICollectionViewFlowLayout

	var collectionViewBinding: CollectionViewBinding<HashableSectionProvider<DataProvidingSectionProvider<AnyDataProvider<[MovieListQuery.Data.Movie], Error>>, String>>? = nil
	var collectionViewDelegate: UICollectionViewDelegate? = nil {
		didSet {
			collectionView?.delegate = collectionViewDelegate
		}
	}

	var paginate: AnyCancellable?
	var selectItem: AnyCancellable?

	init(serverController: OlarisServerController) {
		self.serverController = serverController

		let layout = UICollectionViewFlowLayout()
		self.layout = layout
		
		super.init(collectionViewLayout: layout)

		self.title = NSLocalizedString("Movies", comment: "Title of Movies tab")
		self.tabBarItem.image = UIImage(systemName: "film")
		updateFrame()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	func updateFrame() {
		layout.sectionInset = UIEdgeInsets(top: 20, left: 15, bottom: 20, right: 15)

		let frame = view.frame.inset(by: layout.sectionInset)
		let interitemSpacing: CGFloat = 8
		let minWidth: CGFloat = 120

		let calculatedMinCells = floor((frame.width + interitemSpacing) / (minWidth + interitemSpacing))
		let calculatedMinWidth = floor((frame.width + interitemSpacing) / calculatedMinCells - interitemSpacing)

		layout.minimumInteritemSpacing = interitemSpacing
		layout.itemSize = CGSize(width: calculatedMinWidth, height: calculatedMinWidth * 2)
	}

	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		updateFrame()
	}

	lazy var activity: NSUserActivity = ({
		let activity = NSUserActivity(activityType: "OpenMoviesIntent")
		activity.title = NSLocalizedString("Movies", comment: "User activity name for the movies tab")
		activity.isEligibleForSearch = true
		activity.isEligibleForHandoff = true
		activity.isEligibleForPublicIndexing = false
		#if os(iOS)
		activity.isEligibleForPrediction = false
		#endif
		activity.keywords = Set(["movie", "movies", "olaris"])
		return activity
	})()

	#if os(iOS)
	lazy var intent: INIntent = ({
		let intent = OpenMoviesIntent()
		intent.suggestedInvocationPhrase = "Open Movies in Olaris"
		return intent
	})()

	lazy var interaction: INInteraction = ({
		INInteraction(intent: intent, response: nil)
	})()
	#endif

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		activity.becomeCurrent()
		#if os(iOS)
		interaction.donate(completion: nil)
		#endif
	}

    override func viewDidLoad() {
        super.viewDidLoad()

		guard let collectionView = self.collectionView else {
			fatalError()
		}

		collectionView.backgroundColor = OlarisAssets.background

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
		collectionView.register(UINib(nibName: "MovieCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.

		let columns: UInt = 3

		// Set up the data processing pipeline
		let paginator = PaginatingDataProvider(
			initialPageProps: OffsetLimitPagination(start: 0, count: 4 * columns),
			fetchProvider: { props -> GraphQLSelectionSetDataProvider<MovieListQuery> in
				let dataProvider = MovieListQuery(offset: props.start, limit: Int(props.count)) // exports MovieListQuery.Data.Movie
					.arrayDataProvider(serverController: self.serverController)
				return dataProvider
			},
			paging: { (props, items) in
				if (props.end > items.count) {
					return nil
				}
				else {
					return OffsetLimitPagination(start: props.end, count: 8 * columns)
				}
			}
		)

		let sectionProvider = paginator
			.filteringOptionals()
			.deduplicated(by: \.uuid)
			.sorted(by: \.name)
			.asAnyDataProvider
			.sectionProvider(hashing: \.uuid) // wraps into a Hashing<Input, KeyPath.Value>

		// A closure to create the cell for a given movie
		let cellProvider = { (movie: Hashing<MovieListQuery.Data.Movie, String>, indexPath: IndexPath, collectionView: UICollectionView) -> UICollectionViewCell in
			guard let movieCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? MovieCollectionViewCell else { fatalError() }

			movieCell.graphqlClient = self.serverController.graphqlClient
			movieCell.movie = movie.wrapped
			return movieCell
		}

		// Converts changes from the section provider into a diffable collection view data source
		let collectionViewBinding = CollectionViewBinding(
			collectionView: collectionView,
			sectionProvider: sectionProvider,
			cellProvider: cellProvider
		)
		self.collectionViewBinding = collectionViewBinding

		let delegate = CollectionViewPublisherDelegate(collectionView: collectionView)
		collectionViewDelegate = delegate

		paginate = delegate.scrollViewDelegate.scrollState.paginate(
			in: collectionView,
			with: paginator
		)

		selectItem = delegate.selected
			.filter({ (indexPath, selected) -> Bool in
				guard case .selected = selected else {
					return false
				}
				return true
			})
			.map { (indexPath, _) -> IndexPath in indexPath }
			.map { indexPath -> MovieListQuery.Data.Movie? in
				collectionViewBinding.sectionProvider.item(at: indexPath, full: indexPath)?.wrapped
			}
			.filteringOptionals
			.receive(on: DispatchQueue.main)
			.sink { movie in
				#if os(iOS)
//				let viewController = MovieModalViewController(movie: movie, serverController: serverController)
				let viewController = MovieCollectionViewController(movie: movie, serverController: self.serverController)
				viewController.hidesBottomBarWhenPushed = true
				viewController.collectionView.contentInsetAdjustmentBehavior = .never
//				self.present(viewController, animated: true, completion: nil)
				self.navigationController?.pushViewController(viewController, animated: true)
				#endif
			}
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		for indexPath in (collectionView?.indexPathsForSelectedItems ?? []) {
			collectionView?.deselectItem(at: indexPath, animated: true)
		}
	}

	#if os(iOS)
	override var preferredStatusBarStyle: UIStatusBarStyle {
		.lightContent
	}
	#endif
}
