//  VideoPlayerViewController.swift
//  Olaris-iOS
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Apollo
import AVFoundation
import AVKit
import Combine
import Foundation

class VideoPlayerViewController: AVPlayerViewController {
	enum CreateVideoPlayerError: Error {
		case noStreamingURL
		case invalidURLFromComponents(URLComponents)
		case unknown(Error)
	}

	static func create(uuid: String, serverController: OlarisServerController, graphqlClient: GraphQLClient, assetDelegate: AVAssetResourceLoaderDelegate? = nil) -> AnyPublisher<VideoPlayerViewController, CreateVideoPlayerError> {
		Just(CreateStreamingTicketMutation(uuid: uuid))
			// Run the mutation
			.flatMap { mutation in
				Future<GraphQLResult<CreateStreamingTicketMutation.Data>, Error> { handler in
					graphqlClient.apolloClient.perform(mutation: mutation, context: nil, queue: DispatchQueue.global(), resultHandler: handler)
					return
				}
				.ignoreErrors()
		}
			// switch to the latest playback ticket
			// .switchToLatest()
			.map { result in result.data?.createStreamingTicket }
			.filteringOptionals
			.flatMap { (result: CreateStreamingTicketMutation.Data.CreateStreamingTicket) in Publishers.CombineLatest(
				Just(result),
				// fetch the metadata for compatible codecs
				Just(result)
					.map { result in serverController.url(path: result.metadataPath) }
					.filteringOptionals
					.flatMap({ url -> AnyPublisher<Result<(Data, URLResponse), URLError>, Never> in
						graphqlClient.server.session.dataTaskPublisher(for: url)
							.map { Result<(Data, URLResponse), URLError>.success($0) }
							.catch { Just(Result<(Data, URLResponse), URLError>.failure($0)) }
							.ignoreErrors()
					})
					.map { try? $0.get() }
					.filteringOptionals
					.map { (data, _) in data }
					.decode(type: StreamingTicketMetadata.self, decoder: JSONDecoder())
					.ignoreErrors()
					.map({ metadata in
						metadata.checkCodecs.compactMap { AVVideoCodecType(rawValue: $0) }
					})
			).prefix(1) }
			.eraseToAnyPublisher()
			.receive(on: DispatchQueue.main)
			.tryMap { result -> VideoPlayerViewController in
				let (streamingTicket, playableCodecs) = result
				guard
					let baseURL = serverController.url(path: streamingTicket.hlsStreamingPath),
					var components = URLComponents(url: baseURL.absoluteURL, resolvingAgainstBaseURL: false)
					else {
						throw CreateVideoPlayerError.noStreamingURL
				}

				components.queryItems = (components.queryItems ?? []) + playableCodecs.map { URLQueryItem(name: "playableCodecs", value: $0.rawValue) }
				guard let url = components.url else {
					throw CreateVideoPlayerError.invalidURLFromComponents(components)
				}

				let playerViewController = VideoPlayerViewController()

				let player = AVPlayer()
				player.automaticallyWaitsToMinimizeStalling = false

				let asset = AVURLAsset(url: url)
				asset.resourceLoader.setDelegate(assetDelegate, queue: DispatchQueue.main)
				let playerItem = AVPlayerItem(asset: asset)
				playerItem.canUseNetworkResourcesForLiveStreamingWhilePaused = true

				player.automaticallyWaitsToMinimizeStalling = true
				playerViewController.player = player

				player.replaceCurrentItem(with: playerItem)
				player.play()
				return playerViewController
			}
			.mapError({ error -> CreateVideoPlayerError in
				error as? CreateVideoPlayerError ?? .unknown(error)
			})
			.eraseToAnyPublisher()
	}

	override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
		return .landscapeRight
	}
}
