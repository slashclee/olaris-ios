//  SeasonsCollectionViewController.swift
//  Olaris-iOS
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import Foundation
import SDWebImage
import UIKit

private let reuseIdentifier = "SeasonCollectionViewCell"

final class SeasonsCollectionViewController: UICollectionViewController {
	let serverController: OlarisServerController
	let layout: UICollectionViewFlowLayout
	let series: SeriesListQuery.Data.Series

	var collectionViewBinding: CollectionViewBinding<HashableSectionProvider<DataProvidingSectionProvider<AnyDataProvider<[SeriesListQuery.Data.Series.Season], Never>>, String>>? = nil
	var collectionViewDelegate: UICollectionViewDelegate? = nil {
		didSet {
			collectionView.delegate = collectionViewDelegate
		}
	}

	var selectItem: AnyCancellable? = nil

	init(series: SeriesListQuery.Data.Series, serverController: OlarisServerController) {
		self.series = series
		self.serverController = serverController

		let layout = UICollectionViewFlowLayout()
		layout.itemSize = CGSize(width: 130, height: 246)
		self.layout = layout

		super.init(collectionViewLayout: layout)

		self.title = series.name
		updateFrame()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		updateFrame()
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		self.collectionView?.backgroundColor = OlarisAssets.background

		// Uncomment the following line to preserve selection between presentations
		// self.clearsSelectionOnViewWillAppear = false

		// Register cell classes
		self.collectionView?.register(UINib(nibName: "SeasonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)

		// Do any additional setup after loading the view.

		let sectionProvider = StaticDataProvider(series.seasons)
			.filteringOptionals()
			.sorted(by: \.seasonNumber)
			.asAnyDataProvider
			.sectionProvider(hashing: \.uuid)

		let collectionViewBinding = CollectionViewBinding(
			collectionView: self.collectionView,
			sectionProvider: sectionProvider,
			canRefresh: false,
			cellProvider: { (season, indexPath, collectionView) -> UICollectionViewCell in
				let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
				guard let seasonCell = cell as? SeasonCollectionViewCell else {
					assertionFailure()
					return cell
				}
				seasonCell.graphqlClient = self.serverController.graphqlClient
				seasonCell.season = season.wrapped
				return seasonCell
			}
		)
		self.collectionViewBinding = collectionViewBinding

		let delegate = CollectionViewPublisherDelegate(collectionView: collectionView)
		collectionViewDelegate = delegate

		selectItem = delegate.selected
			.filter({ (indexPath, selected) -> Bool in
				guard case .selected = selected else {
					return false
				}
				return true
			})
			.map { (indexPath, _) -> IndexPath in indexPath }
			.map { indexPath -> SeriesListQuery.Data.Series.Season? in
				collectionViewBinding.sectionProvider.item(at: indexPath, full: indexPath)?.wrapped
		}
		.filteringOptionals
		.receive(on: DispatchQueue.main)
		.sink { season in
			let viewController = EpisodesTableViewController(series: self.series, season: season, serverController: self.serverController)
			self.navigationController?.pushViewController(viewController, animated: true)
		}

		sectionProvider.updateNewerIfNeeded()
	}

	func updateFrame() {
		layout.sectionInset = UIEdgeInsets(top: 20, left: 15, bottom: 20, right: 15)

		let frame = view.frame.inset(by: layout.sectionInset)
		let interitemSpacing: CGFloat = 8
		let minWidth: CGFloat = 120

		let calculatedMinCells = floor((frame.width + interitemSpacing) / (minWidth + interitemSpacing))
		let calculatedMinWidth = floor((frame.width + interitemSpacing) / calculatedMinCells - interitemSpacing)

		layout.minimumInteritemSpacing = interitemSpacing
		layout.itemSize = CGSize(width: calculatedMinWidth, height: calculatedMinWidth * 2)
	}

	#if os(iOS)
	override var preferredStatusBarStyle: UIStatusBarStyle {
		.lightContent
	}
	#endif
}
