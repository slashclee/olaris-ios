//
//  ContentView.swift
//  Olaris-iOS
//
//  Copyright (c) 2019 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var serverController: OlarisServerController
    
    var body: some View {
        TabView {
			serverController.graphqlClient.map { graphqlClient in
				Group {
					DashboardTab()
						.tabItem {
							Image(systemName: "gauge").font(.headline)
							Text("Dashboard")
						}
					MoviesTab(
						queryRunner: GraphQLQueryRunner(query: MovieListQuery(), client: graphqlClient.apolloClient, start: true)
						, minimumCellWidth: 100)
						.tabItem {
							Image(systemName: "film").font(.headline)
							Text("Movies")
						}
					SeriesTab(
						queryRunner: GraphQLQueryRunner(query: SeriesListQuery(), client: graphqlClient.apolloClient, start: true)
						, minimumCellWidth: 100)
						.tabItem {
							Image(systemName: "tv").font(.headline)
							Text("TV Shows")
						}
				}.environmentObject(graphqlClient)
			}
		}
        .font(.headline)
		.sheet(isPresented: Binding(get: { self.serverController.isLoggedOut }, set: { _ in })) { () in
			LoginSheet()
				.frame(maxWidth: 400)
				.environmentObject(self.serverController)
		}
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
