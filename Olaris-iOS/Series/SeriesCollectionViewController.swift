//  SeriessCollectionViewController.swift
//  Olaris-iOS
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import Intents
import UIKit

private let reuseIdentifier = "SeriesCollectionViewCell"

class SeriesCollectionViewController: UICollectionViewController {
	let serverController: OlarisServerController
	let layout: UICollectionViewFlowLayout

	var collectionViewBinding: CollectionViewBinding<HashableSectionProvider<DataProvidingSectionProvider<AnyDataProvider<[SeriesListQuery.Data.Series], Error>>, String>>? = nil
	var collectionViewDelegate: UICollectionViewDelegate? = nil {
		didSet {
			collectionView.delegate = collectionViewDelegate
		}
	}

	var selectItem: AnyCancellable?
	var paginate: AnyCancellable? = nil

	init(serverController: OlarisServerController) {
		self.serverController = serverController

		let layout = UICollectionViewFlowLayout()
		self.layout = layout

		super.init(collectionViewLayout: layout)

		self.title = NSLocalizedString("TV Shows", comment: "Title of TV Shows tab")
		self.tabBarItem.image = UIImage(systemName: "tv")
		updateFrame()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	lazy var activity: NSUserActivity = ({
		let activity = NSUserActivity(activityType: "OpenShowsIntent")
		activity.title = NSLocalizedString("TV Shows", comment: "User activity name for the TV shows tab")
		activity.isEligibleForSearch = true
		activity.isEligibleForHandoff = true
		activity.isEligibleForPublicIndexing = false
		activity.isEligibleForPrediction = false
		activity.keywords = Set(["tv", "shows", "television", "show", "olaris"])
		return activity
	})()

	lazy var intent: INIntent = ({
		let intent = OpenMoviesIntent()
		intent.suggestedInvocationPhrase = "Open TV Shows in Olaris"
		return intent
	})()

	lazy var interaction: INInteraction = ({
		INInteraction(intent: intent, response: nil)
	})()

	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		updateFrame()
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		activity.becomeCurrent()
		interaction.donate(completion: nil)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()

		self.collectionView?.backgroundColor = OlarisAssets.background

		// Uncomment the following line to preserve selection between presentations
		// self.clearsSelectionOnViewWillAppear = false

		// Register cell classes
		self.collectionView?.register(UINib(nibName: "SeriesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)

		// Do any additional setup after loading the view.

		let paginator = PaginatingDataProvider(
			initialPageProps: OffsetLimitPagination(start: 0, count: 12),
			fetchProvider: { props -> GraphQLSelectionSetDataProvider<SeriesListQuery> in
				let dataProvider = SeriesListQuery(offset: props.start, limit: props.end) // exports SeriesListQuery.Data.Series
					.arrayDataProvider(serverController: self.serverController)
				return dataProvider
		},
			paging: { (props, items) in
				if (props.end > items.count) {
					return nil
				}
				else {
					return OffsetLimitPagination(start: props.end, count: 24)
				}
		}
		)

		let sectionProvider = paginator
			.filteringOptionals()
			.deduplicated(by: \.uuid)
			.asAnyDataProvider
			.sectionProvider(hashing: \.uuid) // wraps into a Hashing<Input, KeyPath.Value>
		let collectionViewBinding = CollectionViewBinding(
			collectionView: self.collectionView,
			sectionProvider: sectionProvider,
			cellProvider: { (series, indexPath, collectionView) -> UICollectionViewCell in
				let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
				guard let showCell = cell as? SeriesCollectionViewCell else {
					assertionFailure()
					return cell
				}
				showCell.graphqlClient = self.serverController.graphqlClient
				showCell.series = series.wrapped
				return showCell
			}
		)
		self.collectionViewBinding = collectionViewBinding

		let delegate = CollectionViewPublisherDelegate(collectionView: collectionView)
		collectionViewDelegate = delegate

		paginate = delegate.scrollViewDelegate.scrollState.paginate(in: collectionView, with: paginator)

		selectItem = delegate.selected
			.filter({ (indexPath, selected) -> Bool in
				guard case .selected = selected else {
					return false
				}
				return true
			})
			.map { (indexPath, _) -> IndexPath in indexPath }
			.map { indexPath -> SeriesListQuery.Data.Series? in
				collectionViewBinding.sectionProvider.item(at: indexPath, full: indexPath)?.wrapped
		}
		.filteringOptionals
		.receive(on: DispatchQueue.main)
		.sink { series in
			let viewController = SeasonsCollectionViewController(series: series, serverController: self.serverController)
			self.navigationController?.pushViewController(viewController, animated: true)
		}
	}

	func updateFrame() {
		layout.sectionInset = UIEdgeInsets(top: 20, left: 15, bottom: 20, right: 15)

		let frame = view.frame.inset(by: layout.sectionInset)
		let interitemSpacing: CGFloat = 8
		let minWidth: CGFloat = 120

		let calculatedMinCells = floor((frame.width + interitemSpacing) / (minWidth + interitemSpacing))
		let calculatedMinWidth = floor((frame.width + interitemSpacing) / calculatedMinCells - interitemSpacing)

		layout.minimumInteritemSpacing = interitemSpacing
		layout.itemSize = CGSize(width: calculatedMinWidth, height: calculatedMinWidth * 2)
	}

	#if os(iOS)
	override var preferredStatusBarStyle: UIStatusBarStyle {
		.lightContent
	}
	#endif
}
