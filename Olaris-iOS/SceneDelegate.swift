//
//  SceneDelegate.swift
//  Olaris-iOS
//
//  Copyright (c) 2019 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import UIKit
import SwiftUI

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

	let serverController = OlarisServerController ()

	var cancellables: [AnyCancellable] = []

	func windowScene(_ windowScene: UIWindowScene, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
		if shortcutItem.type == "LogOutAction" {
			serverController.logOut()
		}
	}

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
		scene.delegate = self

        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).

		serverController.logOut()

		if let user = OlarisUser.all.first, let (_, _, server) = user.credentials, let url = URL(string: server) {
			let server = OlarisServer(baseURL: url)
			cancellables.append(server.fetchJWT(user: user)
				.receive(on: RunLoop.main)
				.sink(receiveCompletion: { _ in
//					self.start(scene: scene)
				}) { jwt in
					if let client = GraphQLClient(server: server, jwt: jwt) {
						self.serverController.logIn(server: server, user: user, client: client)
					}
				})
		}
//		else {
//		}
		start(scene: scene)
	}

	let useSwiftUI: Bool = false

	func start(scene: UIScene) {
		guard let windowScene = scene as? UIWindowScene else {
			return
		}

		let window = UIWindow(windowScene: windowScene)
		if useSwiftUI {
			// Use a UIHostingController as window root view controller.
			let contentView = ContentView()
				.environmentObject(serverController)
			let hostingController = UIHostingController(rootView: contentView)
			window.rootViewController = hostingController
		}
		else {
			let movies: UIViewController = ({
				let rootVC = MoviesCollectionViewController(serverController: serverController)
				let nav = StatusBarForwardingNavigationController(navigationBarClass: nil, toolbarClass: nil)
				nav.navigationBar.scrollEdgeAppearance = OlarisAssets.largeTitleNavigationAppearance
				nav.navigationBar.standardAppearance = OlarisAssets.standardNavigationAppearance
				nav.viewControllers = [rootVC]
				nav.navigationBar.prefersLargeTitles = true
				return nav
			})()
			let shows: UIViewController = ({
				let rootVC = SeriesCollectionViewController(serverController: serverController)
				let nav = StatusBarForwardingNavigationController(navigationBarClass: nil, toolbarClass: nil)
				nav.navigationBar.scrollEdgeAppearance = OlarisAssets.largeTitleNavigationAppearance
				nav.navigationBar.standardAppearance = OlarisAssets.standardNavigationAppearance
				nav.viewControllers = [rootVC]
				nav.navigationBar.prefersLargeTitles = true
				nav.view.backgroundColor = OlarisAssets.background
				return nav
			})()
			let tabBar = UITabBarController()
			let appearance = UITabBarAppearance()
			appearance.configureWithOpaqueBackground()
			appearance.backgroundColor = OlarisAssets.navigationBackground
			appearance.backgroundEffect = nil
			tabBar.tabBar.standardAppearance = appearance
			tabBar.tabBar.tintColor = OlarisAssets.navigationHighlighted
			tabBar.viewControllers = [movies, shows]
			tabBar.modalPresentationStyle = .fullScreen
			tabBar.modalPresentationCapturesStatusBarAppearance = true
			window.rootViewController = tabBar
			window.tintColor = .systemPurple

			let loggedIn = serverController.objectWillChange
				.prepend(())
				.map { self.serverController }
				.map(\.isLoggedOut)
				.removeDuplicates()
				.debounce(for: 0.1, scheduler: DispatchQueue.main)
				.scan((nil, nil)) { (input, loggedOut) -> (UIViewController?, UIViewController?) in
					let (_, previous) = input
					if loggedOut {
						let vc = UIHostingController(rootView: LoginSheet().environmentObject(self.serverController))
						vc.modalPresentationStyle = .pageSheet	
						return (previous, vc)
					}
					else {
						return (previous, nil)
					}
				}
			.eraseToAnyPublisher()

			cancellables.append(loggedIn
				.sink(receiveValue: { value in
					let (previous, next) = value
					let showNext = { () -> Void in
						if let next = next {
							tabBar.present(next, animated: true, completion: nil)
						}
					}
					if let previous = previous {
						previous.dismiss(animated: true, completion: showNext)
					}
					else {
						showNext()
					}
				})
			)
		}
		self.window = window
		window.makeKeyAndVisible()
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }

	func scene(_ scene: UIScene, continue userActivity: NSUserActivity) {
		print("GOOOOO \(userActivity)")
		if useSwiftUI {
			assertionFailure()
		}

		guard
			let tabBar = self.window?.rootViewController as? UITabBarController
		else {
				return
		}

		if
			userActivity.activityType == "OpenMoviesIntent" || userActivity.activityType == "OpenMoviesTabIntent",
			let tabIndex = tabBar.viewControllers?.firstIndex(where: {
				($0 as? UINavigationController)?.viewControllers.first is MoviesCollectionViewController
			})
		{
			tabBar.selectedIndex = tabIndex
		}

		if
			userActivity.activityType == "OpenShowsIntent" || userActivity.activityType == "OpenShowsTabIntent",
			let tabIndex = tabBar.viewControllers?.firstIndex(where: {
				($0 as? UINavigationController)?.viewControllers.first is SeriesCollectionViewController
			})
		{
			tabBar.selectedIndex = tabIndex
		}
	}
}

class StatusBarForwardingNavigationController: UINavigationController {
	override open var childForStatusBarStyle: UIViewController? {
		return self.topViewController
	}
}
