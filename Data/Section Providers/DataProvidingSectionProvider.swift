//  DataProvidingSectionProvider.swift
//  Olaris
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import Foundation

class DataProvidingSectionProvider<Provider: DataProvider>: SectionProvider, ObservableObject where Provider.ObjectType: Collection, Provider.ObjectType.Index == Int {
	func item(at indexPath: IndexPath, full fullndexPath: IndexPath) -> Provider.ObjectType.Element? {
		guard indexPath.section == 0, let items = self.items else {
			return nil
		}
		return items[indexPath.item]
	}

	var numberOfSections: Int {
		1
	}

	func numberOfItems(in section: Int) -> Int {
		guard let items = self.items else {
			return 0
		}
		return items.count
	}

	typealias ObjectType = Provider.ObjectType.Element

	typealias ObjectWillChangePublisher = AnyPublisher<DataProvidingSectionProvider<Provider>, Never>
	lazy var objectWillChange: AnyPublisher<DataProvidingSectionProvider<Provider>, Never> = {
		dataProvider.objectWillChange
			.map { _ in self }
			.eraseToAnyPublisher()
	}()

	let dataProvider: Provider
	var items: Provider.ObjectType? {
		dataProvider.status.data
	}

	init(dataProvider: Provider) {
		self.dataProvider = dataProvider
	}

	func updateNewerIfNeeded() {
		dataProvider.fetch()
	}

	var updating: Bool {
		dataProvider.status.loading
	}
}

extension DataProvider where ObjectType: Collection, ObjectType.Index == Int {
	var sectionProvider: DataProvidingSectionProvider<Self> {
		DataProvidingSectionProvider(dataProvider: self)
	}

	func sectionProvider<T>(hashing keyPath: KeyPath<ObjectType.Element, T>) -> HashableSectionProvider<DataProvidingSectionProvider<Self>, T> {
		sectionProvider.hashing(with: keyPath)
	}
}
