//  StaticSectionProvider.swift
//  Olaris-iOS
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import Foundation

final class StaticSectionDataProvider<Provider: DataProvider, T>: SectionProvider {
	typealias ObjectType = T

	let provider: Provider
	let fields: (Provider.Status) -> [[ObjectType]]
	init(provider: Provider, fields: @escaping (Provider.Status) -> [[ObjectType]]) {
		self.provider = provider
		self.fields = fields

		self.mappedFields = fields(provider.status)
		cancellable = provider.objectWillChange
			.map(fields)
			.assign(to: \.mappedFields, on: self)
	}

	private var mappedFields: [[ObjectType]] {
		didSet  {
			objectWillChange.send()
		}
	}
	private var cancellable: AnyCancellable?

	var numberOfSections: Int {
		mappedFields.count
	}

	func numberOfItems(in section: Int) -> Int {
		mappedFields[section].count
	}

	func item(at indexPath: IndexPath, full fullndexPath: IndexPath) -> T? {
		mappedFields[indexPath.section][indexPath.item]
	}

	func updateNewerIfNeeded() {
		provider.fetch()
	}

	var updating: Bool {
		provider.status.loading
	}
}
