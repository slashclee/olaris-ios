//  CombinedSectionProvider.swift
//  Olaris
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import Foundation

enum OneOf<T1, T2> {
	case one(T1)
	case two(T2)
}

class CombinedSectionProvider<Provider1: SectionProvider, Provider2: SectionProvider>: SectionProvider, ObservableObject {
	typealias ObjectType = OneOf

	private let provider1: Provider1
	private let provider2: Provider2

	typealias ObjectWillChangePublisher = AnyPublisher<CombinedSectionProvider<Provider1, Provider2>, Never>
	lazy var objectWillChange: AnyPublisher<CombinedSectionProvider<Provider1, Provider2>, Never> = {
		provider1.objectWillChange.combineLatest(provider2.objectWillChange)
			.map { (_, _) in self }
			.eraseToAnyPublisher()
	}()

	init(_ p1: Provider1, _ p2: Provider2) {
		self.provider1 = p1
		self.provider2 = p2
	}

	var numberOfSections: Int {
		provider1.numberOfSections + provider2.numberOfSections
	}

	func numberOfItems(in section: Int) -> Int {
		let provider1Sections = provider1.numberOfSections
		let provider2Sections = provider2.numberOfSections
		if section < provider1Sections {
			return provider1.numberOfItems(in: section)
		}
		else if section < provider2Sections {
			return provider2.numberOfItems(in: section - provider1Sections)
		}
		else {
			return 0
		}
	}

	func item(at indexPath: IndexPath, full fullndexPath: IndexPath) -> OneOf<Provider1.ObjectType, Provider2.ObjectType>? {
		let provider1Sections = provider1.numberOfSections
		let provider2Sections = provider2.numberOfSections
		if indexPath.section < provider1Sections {
			let item = provider1.item(at: indexPath, full: fullndexPath)
			return item.map { .one($0) }
		}
		else if indexPath.section < provider2Sections {
			let indexPath = IndexPath(item: indexPath.item, section: indexPath.section - provider1Sections)
			let item = provider2.item(at: indexPath, full: fullndexPath)
			return item.map { .two($0) }
		}
		else {
			return nil
		}
	}

	func updateNewerIfNeeded() {
		provider1.updateNewerIfNeeded()
		provider2.updateNewerIfNeeded()
	}

	var updating: Bool {
		provider1.updating || provider2.updating
	}
}

extension SectionProvider {
	func combined<Provider: SectionProvider>(with other: Provider) -> CombinedSectionProvider<Self, Provider> {
		CombinedSectionProvider(self, other)
	}
}

func +<P1: SectionProvider, P2: SectionProvider>(lhs: P1, rhs: P2) -> CombinedSectionProvider<P1, P2> {
	CombinedSectionProvider(lhs, rhs)
}
