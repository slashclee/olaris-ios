//  HashableSectionProvider.swift
//  Olaris
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import Foundation

struct Hashing<T, V: Hashable>: Hashable {
	static func == (lhs: Hashing<T, V>, rhs: Hashing<T, V>) -> Bool {
		lhs._hashableValue == rhs._hashableValue
	}

	func hash(into hasher: inout Hasher) {
		hasher.combine(_hashableValue)
	}

	private(set) var wrapped: T
	private let hashableKeyPath: KeyPath<T, V>
	private var _hashableValue: V {
		wrapped[keyPath: hashableKeyPath]
	}

	init(_ wrapped: T, keyPath: KeyPath<T, V>) {
		self.wrapped = wrapped
		self.hashableKeyPath = keyPath
	}

	subscript<Value>(dynamicMember keyPath: WritableKeyPath<T, Value?>) -> Value? {
		get {
			wrapped[keyPath: keyPath]
		}
		set {
			wrapped[keyPath: keyPath] = newValue
		}
	}
}

class HashableSectionProvider<Provider: SectionProvider, Identifier: Hashable>: SectionProvider {
	var numberOfSections: Int {
		provider.numberOfSections
	}

	func numberOfItems(in section: Int) -> Int {
		provider.numberOfItems(in: section)
	}

	func item(at indexPath: IndexPath, full fullndexPath: IndexPath) -> ObjectType? {
		guard let item = provider.item(at: indexPath, full: indexPath) else {
			return nil
		}
		return Hashing(item, keyPath: keyPath)
	}

	typealias Path = KeyPath<Provider.ObjectType, Identifier>
	typealias ObjectType = Hashing<Provider.ObjectType, Identifier>

	typealias ObjectWillChangePublisher = AnyPublisher<HashableSectionProvider<Provider, Identifier>, Never>
	lazy var objectWillChange: AnyPublisher<HashableSectionProvider<Provider, Identifier>, Never> = {
		provider.objectWillChange
			.map { _ in self }
			.eraseToAnyPublisher()
	}()

	let provider: Provider
	let keyPath: Path

	init(provider: Provider, keyPath: Path) {
		self.provider = provider
		self.keyPath = keyPath
	}

	func updateNewerIfNeeded() {
		provider.updateNewerIfNeeded()
	}

	var updating: Bool {
		provider.updating
	}
}

extension SectionProvider {
	func hashing<T: Hashable>(with keyPath: KeyPath<Self.ObjectType, T>) -> HashableSectionProvider<Self, T> {
		HashableSectionProvider(provider: self, keyPath: keyPath)
	}
}
