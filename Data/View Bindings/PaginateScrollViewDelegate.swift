//  PaginateScrollViewDelegate.swift
//  Olaris
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import UIKit

final class PaginateScrollViewDelegate<BaseProvider: DataProvider, Props>: NSObject, UIScrollViewDelegate, UITableViewDelegate, UICollectionViewDelegate where BaseProvider.ObjectType: Collection {
	typealias Provider = PaginatingDataProvider<BaseProvider, Props>

	let provider: Provider
	init(provider: Provider) {
		self.provider = provider
	}

	private(set) var didPage = false
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		guard !didPage && !provider.status.loading && provider.canPage else {
			return
		}

		let currentOffset = scrollView.contentOffset.y
		let maximumOffset = scrollView.contentSize.height - scrollView.bounds.size.height
		let delta = maximumOffset - currentOffset
		if delta < 0 {
			provider.fetchNext()
			didPage = true
		}
	}

	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		didPage = false
	}
}

extension Publisher where Self.Output == ScrollViewPublisherDelegate.ScrollState {
	func paginate(
		in scrollView: UIScrollView,
		canPage: @escaping () -> Bool = { () in true },
		page:    @escaping () -> Void = { () in }
	) -> AnyCancellable {
		var didPage: Bool = false

		let states = self.ignoreErrors().share()

		let base = states
			.filter { _ in !didPage && canPage() }
			.filter { scrollState in
				let distance = scrollState.distanceFromBottom - (scrollState.bounds.size.height * 2) < 0
				return distance
			}
			.share()

		let handleScroll = base
			.sink { _ in
				didPage = true
				page()
			}

		var isCheckingDeceleration: Bool = false
		let handleDecelerate = base
			.filter { _ in !isCheckingDeceleration 	}
			.handleEvents(receiveOutput: { _ in
				isCheckingDeceleration = true
			})
			.flatMap({ state in
				states
					.filter { state in !state.isDecelerating }
					.prefix(1)
			})
			.sink(receiveValue: { _ in
				didPage = false
				isCheckingDeceleration = false
			}
		)

		return AnyCancellable {
			handleScroll.cancel()
			handleDecelerate.cancel()
		}
	}

	func paginate<T,U>(in scrollView: UIScrollView, with paginator: PaginatingDataProvider<T, U>) -> AnyCancellable {
		paginate(in: scrollView, canPage: { () in paginator.canPage }, page: { () in paginator.fetchNext() })
	}
}
