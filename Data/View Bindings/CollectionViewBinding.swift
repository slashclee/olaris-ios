//  CollectionViewBinding.swift
//  Olaris-iOS
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import UIKit

final class CollectionViewBinding<Provider: SectionProvider>: NSObject, UICollectionViewDataSource where Provider.ObjectType: Hashable {
	typealias CellProvider = (Provider.ObjectType, IndexPath, UICollectionView) -> UICollectionViewCell

	let collectionView: UICollectionView
	let sectionProvider: Provider
	let cellProvider: CellProvider
	#if os(iOS)
	let refreshControl = UIRefreshControl()
	#endif
	let canRefresh: Bool

	private var cancellable: AnyCancellable? = nil

	private var dataSource: UICollectionViewDiffableDataSource<Int, Provider.ObjectType>?

	init(collectionView: UICollectionView, sectionProvider: Provider, canRefresh: Bool = true, cellProvider: @escaping CellProvider) {
		self.collectionView = collectionView
		self.sectionProvider = sectionProvider
		self.cellProvider = cellProvider
		self.canRefresh = canRefresh

		super.init()

		cancellable = sectionProvider.objectWillChange
			.receive(on: DispatchQueue.main)
			.debounce(for: 0.1, scheduler: DispatchQueue.main)
			.sink(receiveValue: { _ in
				self.reloadData()
			})

		dataSource = UICollectionViewDiffableDataSource(collectionView: collectionView, cellProvider: { (collectionView, indexPath, element) -> UICollectionViewCell? in
			cellProvider(element, indexPath, collectionView)
		})

		reloadData()

		#if os(iOS)
		if canRefresh {
			refreshControl.addTarget(self, action: #selector(updateNewerIfNeeded), for: .valueChanged)
			collectionView.refreshControl = refreshControl
		}
		#endif
	}

	@objc func updateNewerIfNeeded() {
		self.sectionProvider.updateNewerIfNeeded()
	}

	func reloadData() {
		assert(Thread.isMainThread)

		var snapshot = NSDiffableDataSourceSnapshot<Int, Provider.ObjectType>()
		snapshot.appendSections(Array((0..<sectionProvider.numberOfSections)))
		for section in (0..<sectionProvider.numberOfSections) {
			let items = sectionProvider.numberOfItems(in: section)
			guard items > 0 else {
				continue
			}
			let appendedItems = Array(0..<items)
				.map { item in IndexPath(item: item, section: section) }
				.compactMap { indexPath in sectionProvider.item(at: indexPath, full: indexPath) }
			snapshot.appendItems(appendedItems, toSection: section)
		}
		dataSource?.apply(snapshot)

		#if os(iOS)
		if refreshControl.isRefreshing && !sectionProvider.updating {
			refreshControl.endRefreshing()
		}
		else if !refreshControl.isRefreshing && sectionProvider.updating {
			refreshControl.beginRefreshing()
		}
		#endif
	}

	func numberOfSections(in collectionView: UICollectionView) -> Int {
		sectionProvider.numberOfSections
	}

	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		sectionProvider.numberOfItems(in: section)
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		guard let element = sectionProvider.item(at: indexPath, full: indexPath) else {
			fatalError()
		}
		return cellProvider(element, indexPath, collectionView)

	}

}
