//  GraphQLQueryDataProvider.swift
//  Olaris-iOS
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Apollo
import Combine
import Foundation

final class GraphQLQueryDataProvider<Query: GraphQLQuery>: DataProvider {
	typealias ObjectType = Query.Data
	typealias ErrorType = Error

	typealias ObjectWillChangePublisher = AnyPublisher<Status, Never>

	private(set) var status: Status = .unloaded {
		didSet {
			statusSubject.send(status)
		}
	}

	let statusSubject = CurrentValueSubject<Status, Never>(.unloaded)
	lazy var objectWillChange: ObjectWillChangePublisher = {
		statusSubject.eraseToAnyPublisher()
	}()

	var results: Query.Data? {
		switch status {
		case .loaded(let data):
			return data
		default:
			return nil
		}
	}

	private var queryCancellable: AnyCancellable? = nil

	private let serverController: OlarisServerController
	private let query: Query

	private var fetchSubject = PassthroughSubject<Void, Never>()

	init(serverController: OlarisServerController, query: Query) {
		self.serverController = serverController
		self.query = query

		// when the server controller's state changes
		queryCancellable = serverController.objectWillChange
			// or the fetch subject is fired
			.merge(with: fetchSubject.eraseToAnyPublisher())
			// grab our latest state and make sure we're not already loading
			.flatMap({ _ in self.objectWillChange.first() })
			.filter { status in !status.loading }
			// if not, graph the apollo client
			.map { _ in self.serverController }
			.compactMap { $0.graphqlClient?.apolloClient }
			// run the query
			.map { GraphQLQueryRunner(query: self.query, client: $0, start: true) }
			.map { query in query.objectWillChange.map { () in query } }
			// only use the latest one
			.switchToLatest()
			// get the results
			.map { $0.status }
			.assign(to: \.status, on: self)
	}

	func fetch() {
		if case .loading = status {
			return
		}
		fetchSubject.send(())


	}
}

final class GraphQLSelectionSetDataProvider<Query: GraphQLQuery>: DataProvider where Query.Data: GraphQLSelectionSetDataProviding {
	var status: Status {
		switch queryDataProvider.status {
		case .unloaded:
			return .unloaded
		case .loading(let data):
			return .loading(data?.results)
		case .loaded(let data):
			return .loaded(data.results)
		case .failed(let data, let error):
			return .failed(data?.results, error)
		}
	}

	typealias ObjectWillChangePublisher = AnyPublisher<Status, Never>
	typealias QueryDataProvider = GraphQLQueryDataProvider<Query>
	typealias ObjectType = Query.Data.ObjectType
	typealias ErrorType = QueryDataProvider.ErrorType
	typealias Output = Status

	let objectWillChange: AnyPublisher<Status, Never>

	let queryDataProvider: QueryDataProvider
	init(queryDataProvider: QueryDataProvider) {
		self.queryDataProvider = queryDataProvider
		self.objectWillChange = queryDataProvider.objectWillChange
			.map { (status) -> Status in
				switch status {
				case .unloaded:
					return .unloaded
				case .loading(let data):
					return .loading(data?.results)
				case .loaded(let data):
					return .loaded(data.results)
				case .failed(let data, let error):
					return .failed(data?.results, error)
				}
			}
			.eraseToAnyPublisher()
	}

	func fetch() {
		queryDataProvider.fetch()
	}
}

extension GraphQLQueryDataProvider where ObjectType: GraphQLSelectionSetDataProviding {
	var withSelectionSet: GraphQLSelectionSetDataProvider<Query> {
		return GraphQLSelectionSetDataProvider(queryDataProvider: self)
	}
}

extension GraphQLQuery {
	func dataProvider(serverController: OlarisServerController) -> GraphQLQueryDataProvider<Self> {
		GraphQLQueryDataProvider(serverController: serverController, query: self)
	}
}

extension GraphQLQuery where Self.Data: GraphQLSelectionSetDataProviding, Self.Data.ObjectType: Collection {
	func arrayDataProvider(serverController: OlarisServerController) -> GraphQLSelectionSetDataProvider<Self> {
		dataProvider(serverController: serverController).withSelectionSet
	}

	func nonOptionalArrayDataProvider<T>(serverController: OlarisServerController) -> OptionalFilteringDataProvider<T, GraphQLSelectionSetDataProvider<Self>> where Self.Data.ObjectType.Element == Optional<T> {
		let foo = arrayDataProvider(serverController: serverController)
		let bar = foo.filteringOptionals()
		return bar
	}
}
