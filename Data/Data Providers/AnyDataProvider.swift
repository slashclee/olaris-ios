//  AnyDataProvider.swift
//  Olaris
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import Foundation

final class AnyDataProvider<T, E: Error>: DataProvider {
	typealias ObjectType = T
	typealias ErrorType = E

	typealias ObjectWillChangePublisher = AnyPublisher<DataProviderStatus<T, E>, Never>

	private let getFetch: () -> ()
	private let getStatus: () -> DataProviderStatus<T, E>
	let objectWillChange: AnyPublisher<DataProviderStatus<T, E>, Never>

	init<P: DataProvider>(dataProvider: P) where P.ObjectType == T, P.ErrorType == E {
		getStatus = { () in dataProvider.status }
		getFetch = dataProvider.fetch
		objectWillChange = dataProvider.objectWillChange.eraseToAnyPublisher()
	}

	var status: DataProviderStatus<T, E> {
		getStatus()
	}

	func fetch() {
		getFetch()
	}
}

extension DataProvider {
	var asAnyDataProvider: AnyDataProvider<ObjectType, ErrorType> {
		AnyDataProvider(dataProvider: self)
	}
}
