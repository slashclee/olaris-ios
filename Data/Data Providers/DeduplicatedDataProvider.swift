//  DeduplicatedDataProvider.swift
//  Olaris
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import Foundation

final class DeduplicatedDataProvider<Provider: DataProvider, Identifier: Equatable>: DataProvider where Provider.ObjectType: Collection {

	typealias Status = DataProviderStatus<ObjectType, ErrorType>
	typealias ObjectWillChangePublisher = AnyPublisher<Status, Never>
	typealias ObjectType = [Provider.ObjectType.Element]
	typealias ErrorType = Provider.ErrorType
	typealias Element = Provider.ObjectType.Element

	let provider: Provider
	let keyPath: KeyPath<Element, Identifier>

	init(provider: Provider, identifier keyPath: KeyPath<Element, Identifier>) {
		self.provider = provider
		self.keyPath = keyPath
	}

	var objectWillChange: ObjectWillChangePublisher {
		provider.objectWillChange.map { self.deduplicated(status: $0) }.eraseToAnyPublisher()
	}

	func deduplicated(status: DataProviderStatus<Provider.ObjectType, Provider.ErrorType>) -> Status {
		switch status {
		case .unloaded:
			return .unloaded
		case .loading(let collection):
			return .loading(collection?.deduplicated(by: keyPath))
		case .loaded(let collection):
			return .loaded(collection.deduplicated(by: keyPath))
		case .failed(let collection, let error):
			return .failed(collection?.deduplicated(by: keyPath), error)
		}
	}

	var status: Status {
		deduplicated(status: provider.status)
	}

	func fetch() {
		provider.fetch()
	}
}

extension DataProvider where Self.ObjectType: Collection {
	func deduplicated<Identifier: Equatable>(by keyPath: KeyPath<Self.ObjectType.Element, Identifier>) -> DeduplicatedDataProvider<Self, Identifier> {
		DeduplicatedDataProvider(provider: self, identifier: keyPath)
	}
}
