//  PaginatingDataProvider.swift
//  Olaris
//
//  Copyright (c) 2020 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine
import Foundation

final class PaginatingDataProvider<Provider: DataProvider, PaginationProps> : DataProvider where Provider.ObjectType: Collection {
	typealias ObjectType = [Provider.ObjectType.Element]
	typealias ErrorType = Provider.ErrorType

	typealias Status = DataProviderStatus<ObjectType, ErrorType>
	typealias FetchProvider = (PaginationProps) -> Provider
	typealias Paging = (PaginationProps, ObjectType) -> PaginationProps?

	private(set) var status: Status = .unloaded {
		didSet {
			statusChanged.send(status)
		}
	}

	private(set) var statusChanged = PassthroughSubject<Status, Never>()
	lazy var objectWillChange = ({
		statusChanged.eraseToAnyPublisher()
	})()

	let initialPageProps: PaginationProps
	let fetchProvider: FetchProvider
	let paging: Paging

	init(initialPageProps: PaginationProps, fetchProvider: @escaping FetchProvider, paging: @escaping Paging) {
		self.initialPageProps = initialPageProps
		self.fetchProvider = fetchProvider
		self.paging = paging

		fetch()
	}

	func fetch() {
		fetch(page: initialPageProps, reset: true)
	}

	@discardableResult
	func fetchNext() -> Bool {
		guard let nextPage = nextPageProps else {
			return false
		}
		fetch(page: nextPage, reset: false)
		return true
	}

	var canPage: Bool {
		nextPageProps != nil
	}

	private var currentProvider: Provider? = nil
	private var nextPageProps: PaginationProps? = nil
	private var cancellables: [AnyCancellable] = []

	func fetch(page: PaginationProps, reset: Bool) {
		let provider = fetchProvider(page)
		currentProvider = provider

		status = .loading(status.data)

		let publisher = provider.objectWillChange
			.map { newStatus -> Status in
				let currentStatus = self.status
				let currentResults = (!reset ? currentStatus.data : nil) ?? []
				switch newStatus {
				case .unloaded:
					return currentStatus
				case .loading(let newItems):
					var results = currentResults
					if let items = newItems {
						results += items
					}
					return .loading(results)
				case .loaded(let newItems):
					var results = currentResults
					results += newItems
					return .loaded(results)
				case .failed(_, let error):
					return .failed(currentStatus.data, error)
				}
			}
			.share()

		cancellables.append(publisher.sink { status in
			if let items = status.data {
				self.nextPageProps = self.paging(page, items)
			}
			else {
				self.nextPageProps = nil
			}
			self.status = status
		})

		provider.fetch()
	}
}
