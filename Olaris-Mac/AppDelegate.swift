//
//  AppDelegate.swift
//  Olaris
//
//  Copyright (c) 2019 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Apollo
import Cocoa
import Combine
import SwiftUI

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate, DependencyFinder {
	var dependencyParent: DependencyFinder? {
		return nil
	}

    var window: NSWindow!

	let serverController = OlarisServerController()
	var cancellables: [AnyCancellable] = []

    func applicationDidFinishLaunching(_ aNotification: Notification) {
		guard OlarisUser.canAccessKeychain else {
			return assertionFailure()
		}

		serverController.logOut()

		if let user = OlarisUser.all.first, let (_, _, server) = user.credentials, let url = URL(string: server) {
			let server = OlarisServer(baseURL: url)
			cancellables.append(server.fetchJWT(user: user)
				.receive(on: RunLoop.main)
				.sink(receiveCompletion: { _ in
					self.start()
				}) { jwt in
					if let client = GraphQLClient(server: server, jwt: jwt) {
						self.serverController.logIn(server: server, user: user, client: client)
					}
			})
		}
		else {
			start()
		}
	}

	func start() {
        // Create the SwiftUI view that provides the window contents.
        let contentView = ContentView()
			.environmentObject(serverController)
        
        // Create the window and set the content view. 
        window = NSWindow(
            contentRect: NSRect(x: 0, y: 0, width: 480, height: 300),
            styleMask: [.titled, .closable, .miniaturizable, .resizable, .fullSizeContentView],
            backing: .buffered, defer: false)
        window.center()
        window.setFrameAutosaveName("Main Window")
        window.contentView = NSHostingView(rootView: contentView)
        window.makeKeyAndOrderFront(nil)
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

