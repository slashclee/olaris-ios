//
//  CombineExtensions.swift
//  Olaris
//
//  Copyright (c) 2019 Olaris.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import Combine

extension Publisher {
    func ignoreErrors() -> AnyPublisher<Self.Output, Never> {
        tryCatch { _ in
            Empty(completeImmediately: true, outputType: Self.Output.self, failureType: Never.self)
        }
        .assertNoFailure()
        .eraseToAnyPublisher()
    }
}

protocol OptionalConvertible {
	associatedtype Wrapped
	var asOptional: Optional<Wrapped> { get }
}

extension Optional: OptionalConvertible {
	var asOptional: Optional<Wrapped> { return self }
}

extension Publisher where Output: OptionalConvertible {
	var filteringOptionals: AnyPublisher<Output.Wrapped, Failure> {
		self
			.map { $0.asOptional }
			.flatMap { (value) -> AnyPublisher<Output.Wrapped, Failure> in
				if let found = value {
					return Just<Output.Wrapped>(found)
						.setFailureType(to: Failure.self)
						.eraseToAnyPublisher()
				}
				else {
					return Empty<Output.Wrapped, Failure>().eraseToAnyPublisher()
				}
			}
			.eraseToAnyPublisher()
	}
}

extension Sequence {
	func sorted<U: Comparable>(by keyPath: KeyPath<Element, U>) -> [Element] {
		sorted { (lhs, rhs) -> Bool in
			lhs[keyPath: keyPath] < rhs[keyPath: keyPath]
		}
	}

	func deduplicated<I: Equatable>(by keyPath: KeyPath<Element, I>) -> [Element] {
		reduce([]) { (matches, element) -> [Element] in
			if matches.contains(where: { $0[keyPath: keyPath] == element[keyPath: keyPath] }) {
				return matches
			}
			else {
				return matches + [element]
			}
		}
	}
}
